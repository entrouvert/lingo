# lingo - payment and billing system
# Copyright (C) 2023  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.http import HttpResponse
from django.urls import reverse
from django.utils.timezone import now
from django.utils.translation import gettext_lazy as _
from django.utils.translation import pgettext
from django.views.generic import CreateView, DeleteView, DetailView, ListView, UpdateView

from lingo.export_import.views import WithApplicationsMixin
from lingo.invoicing.forms import NewPayerForm, PayerForm, PayerMappingForm
from lingo.invoicing.models import Payer
from lingo.manager.utils import StaffRequiredMixin
from lingo.snapshot.models import PayerSnapshot
from lingo.snapshot.views import InstanceWithSnapshotHistoryCompareView, InstanceWithSnapshotHistoryView
from lingo.utils.misc import json_dump


class PayersListView(StaffRequiredMixin, WithApplicationsMixin, ListView):
    template_name = 'lingo/invoicing/manager_payer_list.html'
    model = Payer

    def dispatch(self, request, *args, **kwargs):
        self.with_applications_dispatch(request)
        return super().dispatch(request, *args, **kwargs)

    def get_queryset(self):
        return self.with_applications_queryset()

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        return self.with_applications_context_data(context)


payers_list = PayersListView.as_view()


class PayerAddView(StaffRequiredMixin, CreateView):
    template_name = 'lingo/invoicing/manager_payer_form.html'
    model = Payer
    form_class = NewPayerForm

    def get_success_url(self):
        return reverse('lingo-manager-invoicing-payer-detail', args=[self.object.pk])

    def form_valid(self, *args, **kwargs):
        response = super().form_valid(*args, **kwargs)
        self.object.take_snapshot(request=self.request, comment=pgettext('snapshot', 'created'))
        return response


payer_add = PayerAddView.as_view()


class PayerDetailView(StaffRequiredMixin, DetailView):
    template_name = 'lingo/invoicing/manager_payer_detail.html'
    model = Payer

    def get_context_data(self, **kwargs):
        kwargs['payer'] = self.object
        kwargs['regies'] = self.object.regie_set.all()
        return super().get_context_data(**kwargs)


payer_detail = PayerDetailView.as_view()


class PayerEditView(StaffRequiredMixin, UpdateView):
    template_name = 'lingo/invoicing/manager_payer_form.html'
    model = Payer
    form_class = PayerForm

    def get_success_url(self):
        return reverse('lingo-manager-invoicing-payer-detail', args=[self.object.pk])

    def form_valid(self, *args, **kwargs):
        response = super().form_valid(*args, **kwargs)
        self.object.take_snapshot(request=self.request)
        return response


payer_edit = PayerEditView.as_view()


class PayerEditMappingView(StaffRequiredMixin, UpdateView):
    template_name = 'lingo/invoicing/manager_payer_mapping_form.html'
    model = Payer
    form_class = PayerMappingForm

    def get_success_url(self):
        return '%s#open:mapping' % reverse('lingo-manager-invoicing-payer-detail', args=[self.object.pk])

    def form_valid(self, *args, **kwargs):
        response = super().form_valid(*args, **kwargs)
        self.object.take_snapshot(request=self.request, comment=_('changed mapping'))
        return response


payer_edit_mapping = PayerEditMappingView.as_view()


class PayerDeleteView(StaffRequiredMixin, DeleteView):
    template_name = 'lingo/manager_confirm_delete.html'
    model = Payer

    def get_queryset(self):
        return super().get_queryset().filter(regie__isnull=True)

    def get_success_url(self):
        return reverse('lingo-manager-invoicing-payer-list')

    def post(self, *args, **kwargs):
        self.get_object().take_snapshot(request=self.request, deletion=True)
        return super().post(*args, **kwargs)


payer_delete = PayerDeleteView.as_view()


class PayerExport(StaffRequiredMixin, DetailView):
    model = Payer

    def get(self, request, *args, **kwargs):
        response = HttpResponse(content_type='application/json')
        attachment = 'attachment; filename="export_payer_{}_{}.json"'.format(
            self.get_object().slug, now().strftime('%Y%m%d')
        )
        response['Content-Disposition'] = attachment
        json_dump({'payers': [self.get_object().export_json()]}, response, indent=2)
        return response


payer_export = PayerExport.as_view()


class PayerInspectView(StaffRequiredMixin, DetailView):
    template_name = 'lingo/invoicing/manager_payer_inspect.html'
    model = Payer


payer_inspect = PayerInspectView.as_view()


class PayerHistoryView(StaffRequiredMixin, InstanceWithSnapshotHistoryView):
    template_name = 'lingo/invoicing/manager_payer_history.html'
    model = PayerSnapshot
    instance_context_key = 'payer'


payer_history = PayerHistoryView.as_view()


class PayerHistoryCompareView(StaffRequiredMixin, InstanceWithSnapshotHistoryCompareView):
    template_name = 'lingo/invoicing/manager_payer_history_compare.html'
    inspect_template_name = 'lingo/invoicing/manager_payer_inspect_fragment.html'
    model = Payer
    instance_context_key = 'payer'
    history_view = 'lingo-manager-payer-history'


payer_history_compare = PayerHistoryCompareView.as_view()
