# lingo - payment and billing system
# Copyright (C) 2022  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.urls import path

from .views import appearance as appearance_views
from .views import campaign as campaign_views
from .views import cancellation_reason as cancellation_reason_views
from .views import home as home_views
from .views import payer as payer_views
from .views import pool as pool_views
from .views import redirect as redirect_views
from .views import regie as regie_views

urlpatterns = [
    path('import/', home_views.config_import, name='lingo-manager-invoicing-config-import'),
    path('export/', home_views.config_export, name='lingo-manager-invoicing-config-export'),
    path('regies/', regie_views.regies_list, name='lingo-manager-invoicing-regie-list'),
    path('regies/goto', regie_views.regies_goto_reference, name='lingo-manager-invoicing-regie-goto'),
    path(
        'appearance/',
        appearance_views.appearance_settings,
        name='lingo-manager-invoicing-appearance-settings',
    ),
    path(
        'regie/add/',
        regie_views.regie_add,
        name='lingo-manager-invoicing-regie-add',
    ),
    path(
        'regie/<int:pk>/',
        regie_views.regie_detail,
        name='lingo-manager-invoicing-regie-detail',
    ),
    path(
        'regie/<int:pk>/parameters/',
        regie_views.regie_parameters,
        name='lingo-manager-invoicing-regie-parameters',
    ),
    path(
        'regie/<int:pk>/edit/',
        regie_views.regie_edit,
        name='lingo-manager-invoicing-regie-edit',
    ),
    path(
        'regie/<int:pk>/edit/permissions/',
        regie_views.regie_permissions_edit,
        name='lingo-manager-invoicing-regie-permissions-edit',
    ),
    path(
        'regie/<int:pk>/edit/counters/',
        regie_views.regie_counters_edit,
        name='lingo-manager-invoicing-regie-counters-edit',
    ),
    path(
        'regie/<int:pk>/edit/publishing/',
        regie_views.regie_publishing_edit,
        name='lingo-manager-invoicing-regie-publishing-edit',
    ),
    path(
        'regie/<int:pk>/delete/',
        regie_views.regie_delete,
        name='lingo-manager-invoicing-regie-delete',
    ),
    path(
        'regie/<int:pk>/export/',
        regie_views.regie_export,
        name='lingo-manager-invoicing-regie-export',
    ),
    path(
        'regie/<int:regie_pk>/payment-type/add/',
        regie_views.payment_type_add,
        name='lingo-manager-invoicing-payment-type-add',
    ),
    path(
        'regie/<int:regie_pk>/payment-type/<int:pk>/edit/',
        regie_views.payment_type_edit,
        name='lingo-manager-invoicing-payment-type-edit',
    ),
    path(
        'regie/<int:regie_pk>/payment-type/<int:pk>/delete/',
        regie_views.payment_type_delete,
        name='lingo-manager-invoicing-payment-type-delete',
    ),
    path(
        'regie/<int:regie_pk>/campaign/add/',
        campaign_views.campaign_add,
        name='lingo-manager-invoicing-campaign-add',
    ),
    path(
        'regie/<int:regie_pk>/campaign/<int:pk>/',
        campaign_views.campaign_detail,
        name='lingo-manager-invoicing-campaign-detail',
    ),
    path(
        'regie/<int:regie_pk>/campaign/<int:pk>/edit/',
        campaign_views.campaign_edit,
        name='lingo-manager-invoicing-campaign-edit',
    ),
    path(
        'regie/<int:regie_pk>/campaign/<int:pk>/edit/dates/',
        campaign_views.campaign_dates_edit,
        name='lingo-manager-invoicing-campaign-dates-edit',
    ),
    path(
        'regie/<int:regie_pk>/campaign/<int:pk>/edit/invoices/',
        campaign_views.campaign_invoices_edit,
        name='lingo-manager-invoicing-campaign-invoices-edit',
    ),
    path(
        'regie/<int:regie_pk>/campaign/<int:pk>/delete/',
        campaign_views.campaign_delete,
        name='lingo-manager-invoicing-campaign-delete',
    ),
    path(
        'regie/<int:regie_pk>/campaign/<int:pk>/unlock-check/',
        campaign_views.campaign_unlock_check,
        name='lingo-manager-invoicing-campaign-unlock-check',
    ),
    path(
        'regie/<int:regie_pk>/campaign/<int:pk>/finalize/',
        campaign_views.campaign_finalize,
        name='lingo-manager-invoicing-campaign-finalize',
    ),
    path(
        'regie/<int:regie_pk>/campaign/<int:pk>/pool/add/',
        pool_views.pool_add,
        name='lingo-manager-invoicing-pool-add',
    ),
    path(
        'regie/<int:regie_pk>/campaign/<int:pk>/pool/<int:pool_pk>/',
        pool_views.pool_detail,
        name='lingo-manager-invoicing-pool-detail',
    ),
    path(
        'regie/<int:regie_pk>/campaign/<int:pk>/pool/<int:pool_pk>/stop/',
        pool_views.pool_stop,
        name='lingo-manager-invoicing-pool-stop',
    ),
    path(
        'regie/<int:regie_pk>/campaign/<int:pk>/pool/<int:pool_pk>/journal/',
        pool_views.pool_journal,
        name='lingo-manager-invoicing-pool-journal',
    ),
    path(
        'regie/<int:regie_pk>/campaign/<int:pk>/pool/<int:pool_pk>/promote/',
        pool_views.pool_promote,
        name='lingo-manager-invoicing-pool-promote',
    ),
    path(
        'regie/<int:regie_pk>/campaign/<int:pk>/pool/<int:pool_pk>/delete/',
        pool_views.pool_delete,
        name='lingo-manager-invoicing-pool-delete',
    ),
    path(
        'regie/<int:regie_pk>/campaign/<int:pk>/pool/<int:pool_pk>/invoice/<int:invoice_pk>/pdf/',
        pool_views.invoice_pdf,
        name='lingo-manager-invoicing-invoice-pdf',
    ),
    path(
        'ajax/regie/<int:regie_pk>/campaign/<int:pk>/pool/<int:pool_pk>/invoice/<int:invoice_pk>/lines/',
        pool_views.invoice_line_list,
        name='lingo-manager-invoicing-invoice-line-list',
    ),
    path(
        'regie/<int:regie_pk>/campaign/<int:pk>/pool/<int:pool_pk>/credit/<int:credit_pk>/pdf/',
        pool_views.credit_pdf,
        name='lingo-manager-invoicing-credit-pdf',
    ),
    path(
        'ajax/regie/<int:regie_pk>/campaign/<int:pk>/pool/<int:pool_pk>/credit/<int:credit_pk>/lines/',
        pool_views.credit_line_list,
        name='lingo-manager-invoicing-credit-line-list',
    ),
    path(
        'ajax/regie/<int:regie_pk>/campaign/<int:pk>/pool/<int:pool_pk>/line/<int:line_pk>/replay/',
        pool_views.line_replay,
        name='lingo-manager-invoicing-line-replay',
    ),
    path(
        'ajax/regie/<int:regie_pk>/campaign/<int:pk>/pool/<int:pool_pk>/line/<int:line_pk>/<slug:status>/',
        pool_views.line_set_error_status,
        name='lingo-manager-invoicing-line-set-error-status',
    ),
    path(
        'regie/<int:regie_pk>/campaign/<int:pk>/add-corrective-campaign/',
        campaign_views.corrective_campaign_add,
        name='lingo-manager-invoicing-corrective-campaign-add',
    ),
    path(
        'regie/<int:regie_pk>/campaign/<int:pk>/corrective/agendas/add/',
        campaign_views.corrective_campaign_all_agendas_add,
        name='lingo-manager-invoicing-corrective-campaign-all-agendas-add',
    ),
    path(
        'regie/<int:regie_pk>/campaign/<int:pk>/corrective/agenda/<int:agenda_pk>/add/',
        campaign_views.corrective_campaign_agenda_add,
        name='lingo-manager-invoicing-corrective-campaign-agenda-add',
    ),
    path(
        'regie/<int:regie_pk>/non-invoiced-lines/',
        regie_views.non_invoiced_line_list,
        name='lingo-manager-invoicing-non-invoiced-line-list',
    ),
    path(
        'regie/<int:regie_pk>/invoices/',
        regie_views.regie_invoice_list,
        name='lingo-manager-invoicing-regie-invoice-list',
    ),
    path(
        'regie/<int:regie_pk>/invoice/<int:invoice_pk>/pdf/',
        regie_views.regie_invoice_pdf,
        name='lingo-manager-invoicing-regie-invoice-pdf',
    ),
    path(
        'regie/<int:regie_pk>/invoice/<int:invoice_pk>/dynamic/pdf/',
        regie_views.regie_invoice_dynamic_pdf,
        name='lingo-manager-invoicing-regie-invoice-dynamic-pdf',
    ),
    path(
        'regie/<int:regie_pk>/invoice/<int:invoice_pk>/payments/pdf/',
        regie_views.regie_invoice_payments_pdf,
        name='lingo-manager-invoicing-regie-invoice-payments-pdf',
    ),
    path(
        'ajax/regie/<int:regie_pk>/invoice/<int:invoice_pk>/lines/',
        regie_views.regie_invoice_line_list,
        name='lingo-manager-invoicing-regie-invoice-line-list',
    ),
    path(
        'regie/<int:regie_pk>/invoice/<int:invoice_pk>/cancel/',
        regie_views.regie_invoice_cancel,
        name='lingo-manager-invoicing-regie-invoice-cancel',
    ),
    path(
        'regie/<int:regie_pk>/collections/invoices/',
        regie_views.regie_collection_invoice_list,
        name='lingo-manager-invoicing-regie-collection-invoice-list',
    ),
    path(
        'regie/<int:regie_pk>/collections/',
        regie_views.regie_collection_list,
        name='lingo-manager-invoicing-regie-collection-list',
    ),
    path(
        'regie/<int:regie_pk>/collection/add/',
        regie_views.regie_collection_add,
        name='lingo-manager-invoicing-regie-collection-add',
    ),
    path(
        'regie/<int:regie_pk>/collection/<int:pk>/',
        regie_views.regie_collection_detail,
        name='lingo-manager-invoicing-regie-collection-detail',
    ),
    path(
        'regie/<int:regie_pk>/collection/<int:pk>/edit/',
        regie_views.regie_collection_edit,
        name='lingo-manager-invoicing-regie-collection-edit',
    ),
    path(
        'regie/<int:regie_pk>/collection/<int:pk>/validate/',
        regie_views.regie_collection_validate,
        name='lingo-manager-invoicing-regie-collection-validate',
    ),
    path(
        'regie/<int:regie_pk>/collection/<int:pk>/delete/',
        regie_views.regie_collection_delete,
        name='lingo-manager-invoicing-regie-collection-delete',
    ),
    path(
        'regie/<int:regie_pk>/payments/',
        regie_views.regie_payment_list,
        name='lingo-manager-invoicing-regie-payment-list',
    ),
    path(
        'regie/<int:regie_pk>/payment/<int:payment_pk>/pdf/',
        regie_views.regie_payment_pdf,
        name='lingo-manager-invoicing-regie-payment-pdf',
    ),
    path(
        'regie/<int:regie_pk>/payment/<int:payment_pk>/cancel/',
        regie_views.regie_payment_cancel,
        name='lingo-manager-invoicing-regie-payment-cancel',
    ),
    path(
        'regie/<int:regie_pk>/dockets/payments/',
        regie_views.regie_docket_payment_list,
        name='lingo-manager-invoicing-regie-docket-payment-list',
    ),
    path(
        'regie/<int:regie_pk>/dockets/',
        regie_views.regie_docket_list,
        name='lingo-manager-invoicing-regie-docket-list',
    ),
    path(
        'regie/<int:regie_pk>/docket/add/',
        regie_views.regie_docket_add,
        name='lingo-manager-invoicing-regie-docket-add',
    ),
    path(
        'regie/<int:regie_pk>/docket/<int:pk>/',
        regie_views.regie_docket_detail,
        name='lingo-manager-invoicing-regie-docket-detail',
    ),
    path(
        'regie/<int:regie_pk>/docket/<int:pk>/export/ods/',
        regie_views.regie_docket_ods,
        name='lingo-manager-invoicing-regie-docket-ods',
    ),
    path(
        'regie/<int:regie_pk>/docket/<int:pk>/export/pdf/',
        regie_views.regie_docket_pdf,
        name='lingo-manager-invoicing-regie-docket-pdf',
    ),
    path(
        'regie/<int:regie_pk>/docket/<int:pk>/edit/',
        regie_views.regie_docket_edit,
        name='lingo-manager-invoicing-regie-docket-edit',
    ),
    path(
        'regie/<int:regie_pk>/docket/<int:pk>/payment-type/<int:payment_type_pk>/',
        regie_views.regie_docket_payment_type_edit,
        name='lingo-manager-invoicing-regie-docket-payment-type-edit',
    ),
    path(
        'regie/<int:regie_pk>/docket/<int:pk>/validate/',
        regie_views.regie_docket_validate,
        name='lingo-manager-invoicing-regie-docket-validate',
    ),
    path(
        'regie/<int:regie_pk>/docket/<int:pk>/delete/',
        regie_views.regie_docket_delete,
        name='lingo-manager-invoicing-regie-docket-delete',
    ),
    path(
        'regie/<int:regie_pk>/credits/',
        regie_views.regie_credit_list,
        name='lingo-manager-invoicing-regie-credit-list',
    ),
    path(
        'ajax/regie/<int:regie_pk>/credit/<int:credit_pk>/lines/',
        regie_views.regie_credit_line_list,
        name='lingo-manager-invoicing-regie-credit-line-list',
    ),
    path(
        'regie/<int:regie_pk>/credit/<int:credit_pk>/cancel/',
        regie_views.regie_credit_cancel,
        name='lingo-manager-invoicing-regie-credit-cancel',
    ),
    path(
        'regie/<int:regie_pk>/credit/<int:credit_pk>/pdf/',
        regie_views.regie_credit_pdf,
        name='lingo-manager-invoicing-regie-credit-pdf',
    ),
    path(
        'regie/<int:regie_pk>/refunds/',
        regie_views.regie_refund_list,
        name='lingo-manager-invoicing-regie-refund-list',
    ),
    path(
        'regie/<int:regie_pk>/payers/',
        regie_views.regie_payer_list,
        name='lingo-manager-invoicing-regie-payer-list',
    ),
    path(
        'regie/<int:regie_pk>/payer/<str:payer_external_id>/transactions/',
        regie_views.regie_payer_transaction_list,
        name='lingo-manager-invoicing-regie-payer-transaction-list',
    ),
    path(
        'regie/<int:pk>/inspect/',
        regie_views.regie_inspect,
        name='lingo-manager-invoicing-regie-inspect',
    ),
    path(
        'regie/<int:pk>/history/',
        regie_views.regie_history,
        name='lingo-manager-invoicing-regie-history',
    ),
    path(
        'regie/<int:pk>/history/compare/',
        regie_views.regie_history_compare,
        name='lingo-manager-invoicing-regie-history-compare',
    ),
    path('payers/', payer_views.payers_list, name='lingo-manager-invoicing-payer-list'),
    path(
        'payer/add/',
        payer_views.payer_add,
        name='lingo-manager-invoicing-payer-add',
    ),
    path(
        'payer/<int:pk>/',
        payer_views.payer_detail,
        name='lingo-manager-invoicing-payer-detail',
    ),
    path(
        'payer/<int:pk>/edit/',
        payer_views.payer_edit,
        name='lingo-manager-invoicing-payer-edit',
    ),
    path(
        'payer/<int:pk>/mapping/',
        payer_views.payer_edit_mapping,
        name='lingo-manager-invoicing-payer-edit-mapping',
    ),
    path(
        'payer/<int:pk>/delete/',
        payer_views.payer_delete,
        name='lingo-manager-invoicing-payer-delete',
    ),
    path(
        'payer/<int:pk>/export/',
        payer_views.payer_export,
        name='lingo-manager-invoicing-payer-export',
    ),
    path(
        'payer/<int:pk>/inspect/',
        payer_views.payer_inspect,
        name='lingo-manager-invoicing-payer-inspect',
    ),
    path(
        'payer/<int:pk>/history/',
        payer_views.payer_history,
        name='lingo-manager-invoicing-payer-history',
    ),
    path(
        'payer/<int:pk>/history/compare/',
        payer_views.payer_history_compare,
        name='lingo-manager-invoicing-payer-history-compare',
    ),
    path(
        'cancellation-reasons/',
        cancellation_reason_views.reason_list,
        name='lingo-manager-invoicing-cancellation-reason-list',
    ),
    path(
        'cancellation-reason/invoice/add/',
        cancellation_reason_views.invoice_reason_add,
        name='lingo-manager-invoicing-invoice-cancellation-reason-add',
    ),
    path(
        'cancellation-reason/invoice/<int:pk>/edit/',
        cancellation_reason_views.invoice_reason_edit,
        name='lingo-manager-invoicing-invoice-cancellation-reason-edit',
    ),
    path(
        'cancellation-reason/invoice/<int:pk>/delete/',
        cancellation_reason_views.invoice_reason_delete,
        name='lingo-manager-invoicing-invoice-cancellation-reason-delete',
    ),
    path(
        'cancellation-reason/credit/add/',
        cancellation_reason_views.credit_reason_add,
        name='lingo-manager-invoicing-credit-cancellation-reason-add',
    ),
    path(
        'cancellation-reason/credit/<int:pk>/edit/',
        cancellation_reason_views.credit_reason_edit,
        name='lingo-manager-invoicing-credit-cancellation-reason-edit',
    ),
    path(
        'cancellation-reason/credit/<int:pk>/delete/',
        cancellation_reason_views.credit_reason_delete,
        name='lingo-manager-invoicing-credit-cancellation-reason-delete',
    ),
    path(
        'cancellation-reason/payment/add/',
        cancellation_reason_views.payment_reason_add,
        name='lingo-manager-invoicing-payment-cancellation-reason-add',
    ),
    path(
        'cancellation-reason/payment/<int:pk>/edit/',
        cancellation_reason_views.payment_reason_edit,
        name='lingo-manager-invoicing-payment-cancellation-reason-edit',
    ),
    path(
        'cancellation-reason/payment/<int:pk>/delete/',
        cancellation_reason_views.payment_reason_delete,
        name='lingo-manager-invoicing-payment-cancellation-reason-delete',
    ),
    path(
        'redirect/invoice/<uuid:invoice_uuid>/',
        redirect_views.invoice_redirect,
        name='lingo-manager-invoicing-invoice-redirect',
    ),
    path(
        'redirect/invoice/<uuid:invoice_uuid>/pdf/',
        redirect_views.invoice_pdf_redirect,
        name='lingo-manager-invoicing-invoice-pdf-redirect',
    ),
    path(
        'redirect/credit/<uuid:credit_uuid>/',
        redirect_views.credit_redirect,
        name='lingo-manager-invoicing-credit-redirect',
    ),
    path(
        'redirect/credit/<uuid:credit_uuid>/pdf/',
        redirect_views.credit_pdf_redirect,
        name='lingo-manager-invoicing-credit-pdf-redirect',
    ),
    path(
        'redirect/payment/<uuid:payment_uuid>/',
        redirect_views.payment_redirect,
        name='lingo-manager-invoicing-payment-redirect',
    ),
    path(
        'redirect/payment/<uuid:payment_uuid>/pdf/',
        redirect_views.payment_pdf_redirect,
        name='lingo-manager-invoicing-payment-pdf-redirect',
    ),
    path(
        'redirect/refund/<uuid:refund_uuid>/',
        redirect_views.refund_redirect,
        name='lingo-manager-invoicing-refund-redirect',
    ),
]
