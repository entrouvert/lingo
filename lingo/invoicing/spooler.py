# lingo - payment and billing system
# Copyright (C) 2023  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.db import connection
from uwsgidecorators import spool  # pylint: disable=import-error

from lingo.invoicing.models import Campaign, Pool


def set_connection(domain):
    from hobo.multitenant.middleware import TenantMiddleware  # pylint: disable=import-error

    tenant = TenantMiddleware.get_tenant_by_hostname(domain)
    connection.set_tenant(tenant)


@spool
def generate_invoices(args):
    if args.get('domain'):
        # multitenant installation
        set_connection(args['domain'])

    try:
        pool = Pool.objects.get(
            campaign__pk=args['campaign_id'], pk=args['pool_id'], status='registered', draft=True
        )
    except Pool.DoesNotExist:
        return

    pool.generate_invoices()


@spool
def populate_from_draft(args):
    if args.get('domain'):
        # multitenant installation
        set_connection(args['domain'])

    try:
        draft_pool = Pool.objects.get(pk=args['draft_pool_id'], status='completed', draft=True)
    except Pool.DoesNotExist:
        return

    if draft_pool.campaign.pool_set.filter(created_at__gt=draft_pool.created_at, draft=True).exists():
        return

    try:
        final_pool = Pool.objects.get(pk=args['final_pool_id'], status='registered', draft=False)
    except Pool.DoesNotExist:
        return

    final_pool.populate_from_draft(draft_pool)


@spool
def make_assignments(args):
    if args.get('domain'):
        # multitenant installation
        set_connection(args['domain'])

    try:
        campaign = Campaign.objects.get(pk=args['campaign_id'], finalized=True)
    except Campaign.DoesNotExist:
        return

    campaign.make_assignments()
