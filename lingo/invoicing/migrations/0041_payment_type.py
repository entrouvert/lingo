# Generated by Django 3.2.18 on 2023-06-26 07:07

from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ('invoicing', '0040_payment_type'),
    ]

    operations = [
        migrations.RenameField(
            model_name='payment',
            old_name='new_payment_type',
            new_name='payment_type',
        ),
    ]
