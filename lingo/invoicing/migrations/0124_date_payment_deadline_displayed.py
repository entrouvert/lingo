from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('invoicing', '0123_credit_cancellation_reason'),
    ]

    operations = [
        migrations.AddField(
            model_name='campaign',
            name='date_payment_deadline_displayed',
            field=models.DateField(blank=True, null=True, verbose_name='Displayed payment deadline'),
        ),
        migrations.AddField(
            model_name='draftinvoice',
            name='date_payment_deadline_displayed',
            field=models.DateField(null=True, verbose_name='Displayed payment deadline'),
        ),
        migrations.AddField(
            model_name='invoice',
            name='date_payment_deadline_displayed',
            field=models.DateField(null=True, verbose_name='Displayed payment deadline'),
        ),
        migrations.AlterField(
            model_name='campaign',
            name='date_payment_deadline',
            field=models.DateField(verbose_name='Effective payment deadline'),
        ),
        migrations.AlterField(
            model_name='draftinvoice',
            name='date_payment_deadline',
            field=models.DateField(verbose_name='Effective payment deadline'),
        ),
        migrations.AlterField(
            model_name='invoice',
            name='date_payment_deadline',
            field=models.DateField(verbose_name='Effective payment deadline'),
        ),
    ]
