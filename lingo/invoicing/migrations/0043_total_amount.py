from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ('invoicing', '0042_payer_external_id_from_nameid'),
    ]

    operations = []
