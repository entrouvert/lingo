# lingo - payment and billing system
# Copyright (C) 2025  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import copy
import uuid

from django.db import models
from django.template import RequestContext, Template, TemplateSyntaxError, VariableDoesNotExist
from django.utils.text import slugify
from django.utils.translation import gettext_lazy as _

from lingo.export_import.models import WithApplicationMixin
from lingo.invoicing import errors
from lingo.snapshot.models import PayerSnapshot, WithSnapshotManager, WithSnapshotMixin
from lingo.utils.misc import WithInspectMixin, clean_import_data, generate_slug
from lingo.utils.wcs import (
    WCSError,
    get_wcs_dependencies_from_template,
    get_wcs_json,
    get_wcs_matching_card_model,
    get_wcs_services,
)


class Payer(WithSnapshotMixin, WithApplicationMixin, WithInspectMixin, models.Model):
    # mark temporarily restored snapshots
    snapshot = models.ForeignKey(
        PayerSnapshot, on_delete=models.CASCADE, null=True, related_name='temporary_instance'
    )

    label = models.CharField(_('Label'), max_length=150)
    slug = models.SlugField(_('Identifier'), max_length=160, unique=True)
    description = models.TextField(_('Description'), null=True, blank=True)
    carddef_reference = models.CharField(_('Card Model'), max_length=150)
    cached_carddef_json = models.JSONField(blank=True, default=dict)
    payer_external_id_prefix = models.CharField(
        _('Prefix for payer external id'),
        max_length=250,
        blank=True,
    )
    payer_external_id_template = models.CharField(
        _('Template for payer external id'),
        max_length=1000,
        help_text=_('To get payer external id from user external id'),
        blank=True,
    )
    payer_external_id_from_nameid_template = models.CharField(
        _('Template for payer external id from nameid'),
        max_length=1000,
        help_text='{{ cards|objects:"adults"|filter_by_user:nameid|first|get:"id"|default:"" }}',
        blank=True,
    )
    user_fields_mapping = models.JSONField(blank=True, default=dict)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    application_component_type = 'payers'
    application_label_singular = _('Payer')
    application_label_plural = _('Payers')

    objects = WithSnapshotManager()
    snapshots = WithSnapshotManager(snapshots=True)

    class Meta:
        ordering = ['label']

    def __str__(self):
        return self.label

    def save(self, *args, **kwargs):
        if not self.slug:
            self.slug = generate_slug(self)
        super().save(*args, **kwargs)

        if 'update_fields' in kwargs:
            # don't populate the cache
            return

        def populate_cache():
            if self.carddef_reference:
                parts = self.carddef_reference.split(':')
                wcs_key, card_slug = parts[:2]
                wcs_site = get_wcs_services().get(wcs_key)
                try:
                    card_schema = get_wcs_json(
                        wcs_site, 'api/cards/%s/@schema' % card_slug, log_errors='warn'
                    )
                except WCSError:
                    return

                if not card_schema:
                    return

                self.cached_carddef_json = card_schema
                self.save(update_fields=['cached_carddef_json'])

        populate_cache()

    @property
    def base_slug(self):
        return slugify(self.label)

    def get_dependencies(self):
        if self.carddef_reference:
            parts = self.carddef_reference.split(':')
            wcs_key, card_slug = parts[:2]
            wcs_site_url = get_wcs_services().get(wcs_key)['url']
            urls = {
                'export': f'{wcs_site_url}api/export-import/cards/{card_slug}/',
                'dependencies': f'{wcs_site_url}api/export-import/cards/{card_slug}/dependencies/',
                'redirect': f'{wcs_site_url}api/export-import/cards/{card_slug}/redirect/',
            }
            yield {
                'type': 'cards',
                'id': card_slug,
                'text': self.cached_carddef_json.get('name'),
                'urls': urls,
            }
        yield from get_wcs_dependencies_from_template(self.payer_external_id_template)
        yield from get_wcs_dependencies_from_template(self.payer_external_id_from_nameid_template)

    def export_json(self):
        return {
            'label': self.label,
            'slug': self.slug,
            'description': self.description,
            'carddef_reference': self.carddef_reference,
            'payer_external_id_prefix': self.payer_external_id_prefix,
            'payer_external_id_template': self.payer_external_id_template,
            'user_fields_mapping': self.user_fields_mapping,
        }

    def get_inspect_keys(self):
        return ['label', 'slug', 'description']

    def get_settings_inspect_fields(self):
        keys = ['carddef_reference', 'payer_external_id_prefix', 'payer_external_id_template']
        yield from self.get_inspect_fields(keys=keys)

    @classmethod
    def import_json(cls, data, snapshot=None):
        data = copy.deepcopy(data)
        data = clean_import_data(cls, data)
        qs_kwargs = {}
        if snapshot:
            qs_kwargs = {'snapshot': snapshot}
            data['slug'] = str(uuid.uuid4())  # random slug
        else:
            qs_kwargs = {'slug': data['slug']}
        payer, created = cls.objects.update_or_create(defaults=data, **qs_kwargs)
        return created, payer

    @property
    def carddef_name(self):
        if not self.carddef_reference:
            return
        result = get_wcs_matching_card_model(self.carddef_reference)
        if not result:
            return
        return result

    @property
    def carddef_fields(self):
        if not self.cached_carddef_json:
            return
        return {
            f['varname']: f['label']
            for f in self.cached_carddef_json.get('fields')
            if f.get('varname') and f['type'] != 'page'
        }

    @property
    def user_variables(self):
        return [
            ('first_name', _('First name')),
            ('last_name', _('Last name')),
            ('address', _('Address')),
            ('demat', _('Demat')),
            ('direct_debit', _('Direct debit')),
        ]

    @property
    def user_fields(self):
        for key, label in self.user_variables:
            value = ''
            if self.user_fields_mapping.get(key):
                varname = self.user_fields_mapping.get(key)
                value = self.carddef_fields.get(varname) or ''
            yield label, value

    def _get_payer_external_id(self, request, original_context, template_key):
        context = RequestContext(request)
        context.push(original_context)
        tplt = getattr(self, template_key) or ''
        if not tplt:
            raise errors.PayerError(details={'reason': 'empty-template'})
        try:
            value = Template(tplt).render(context)
            if not value:
                raise errors.PayerError(details={'reason': 'empty-result'})
            return '%s%s' % (self.payer_external_id_prefix, value)
        except TemplateSyntaxError:
            raise errors.PayerError(details={'reason': 'syntax-error'})
        except VariableDoesNotExist:
            raise errors.PayerError(details={'reason': 'variable-error'})

    def get_payer_external_id(self, request, original_context):
        return self._get_payer_external_id(
            request=request,
            original_context=original_context,
            template_key='payer_external_id_template',
        )

    def get_payer_external_id_from_nameid(self, request, original_context):
        return self._get_payer_external_id(
            request=request,
            original_context=original_context,
            template_key='payer_external_id_from_nameid_template',
        )

    def get_payer_data(self, request, payer_external_id):
        if not self.carddef_reference:
            raise errors.PayerError(details={'reason': 'missing-card-model'})
        result = {}
        context = RequestContext(request, autoescape=False)
        payer_external_raw_id = None
        if ':' in payer_external_id:
            payer_external_raw_id = payer_external_id.split(':')[1]
        context.push({'payer_external_id': payer_external_raw_id or payer_external_id})
        bool_keys = ['demat', 'direct_debit']
        for key, dummy in self.user_variables:
            if not self.user_fields_mapping.get(key):
                if key not in bool_keys:
                    raise errors.PayerDataError(details={'key': key, 'reason': 'not-defined'})
                tplt = 'False'
            else:
                tplt = (
                    '{{ cards|objects:"%s"|filter_by_internal_id:payer_external_id|include_fields|first|get:"fields"|get:"%s"|default:"" }}'
                    % (
                        self.carddef_reference.split(':')[1],
                        self.user_fields_mapping[key],
                    )
                )
            value = Template(tplt).render(context)
            if not value:
                if key not in bool_keys:
                    raise errors.PayerDataError(details={'key': key, 'reason': 'empty-result'})
                value = False
            if key in bool_keys:
                if value in ('True', 'true', '1'):
                    value = True
                elif value in ('False', 'false', '0'):
                    value = False
                if not isinstance(value, bool):
                    raise errors.PayerDataError(details={'key': key, 'reason': 'not-a-boolean'})
            result[key] = value
        return result
