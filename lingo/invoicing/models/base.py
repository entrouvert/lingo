# lingo - payment and billing system
# Copyright (C) 2025  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import uuid

from django.core.serializers.json import DjangoJSONEncoder
from django.db import models
from django.template.defaultfilters import linebreaksbr
from django.utils.timezone import localtime
from django.utils.translation import gettext_lazy as _

from lingo.utils.misc import shorten_slug

DOCUMENT_MODELS = [
    ('basic', _('Basic')),
    ('middle', _('Middle')),
    ('full', _('Full')),
]


@models.JSONField.register_lookup
class MatchJsonPath(models.lookups.PostgresOperatorLookup):
    lookup_name = 'jsonpath_exists'
    postgres_operator = '@?'
    prepare_rhs = False


def set_numbers(instance, counter_date, counter_kind):
    from lingo.invoicing.models import Counter

    instance.number = Counter.get_count(
        regie=instance.regie,
        name=instance.regie.get_counter_name(counter_date),
        kind=counter_kind,
    )
    instance.formatted_number = instance.regie.format_number(counter_date, instance.number, counter_kind)


def get_cancellation_info(obj):
    result = []
    if not obj.cancelled_at:
        return result
    result.append((_('Cancelled on'), localtime(obj.cancelled_at).strftime('%d/%m/%Y %H:%M')))
    if obj.cancelled_by:
        result.append((_('Cancelled by'), obj.cancelled_by))
    result.append((_('Reason'), obj.cancellation_reason))
    if obj.cancellation_description:
        result.append((_('Description'), linebreaksbr(obj.cancellation_description)))
    return result


class AbstractInvoiceObject(models.Model):
    uuid = models.UUIDField(default=uuid.uuid4, editable=False, unique=True)
    regie = models.ForeignKey('invoicing.Regie', on_delete=models.PROTECT)
    label = models.CharField(_('Label'), max_length=300)
    total_amount = models.DecimalField(max_digits=9, decimal_places=2, default=0)
    payer_external_id = models.CharField(max_length=250)
    payer_first_name = models.CharField(max_length=250)
    payer_last_name = models.CharField(max_length=250)
    payer_address = models.TextField()

    date_invoicing = models.DateField(_('Invoicing date'), null=True)
    date_publication = models.DateField(_('Publication date'))
    pool = models.ForeignKey('invoicing.Pool', on_delete=models.PROTECT, null=True)
    usable = models.BooleanField(default=True)
    previous_invoice = models.ForeignKey('Invoice', null=True, on_delete=models.SET_NULL)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True, null=True)

    class Meta:
        abstract = True

    @property
    def payer_name(self):
        payer_name = '%s %s' % (self.payer_first_name, self.payer_last_name)
        return payer_name.strip()

    @property
    def payer_external_raw_id(self):
        if ':' in self.payer_external_id:
            return self.payer_external_id.split(':')[1]
        return self.payer_external_id

    @property
    def invoice_model(self):
        if self.pool:
            return self.pool.campaign.invoice_model
        return self.regie.invoice_model


class AbstractInvoiceLineObject(models.Model):
    uuid = models.UUIDField(default=uuid.uuid4, editable=False, unique=True)
    event_date = models.DateField()
    slug = models.CharField(max_length=1000)
    label = models.TextField()
    quantity = models.DecimalField(max_digits=9, decimal_places=2)
    unit_amount = models.DecimalField(max_digits=9, decimal_places=2)
    total_amount = models.DecimalField(max_digits=9, decimal_places=2)
    description = models.TextField()
    event_slug = models.CharField(max_length=250)
    event_label = models.CharField(max_length=260)
    agenda_slug = models.CharField(max_length=250)
    activity_label = models.CharField(max_length=250)
    details = models.JSONField(default=dict, encoder=DjangoJSONEncoder)
    accounting_code = models.CharField(max_length=250, blank=True)
    form_url = models.URLField(blank=True)

    user_external_id = models.CharField(max_length=250)
    user_first_name = models.CharField(max_length=250)
    user_last_name = models.CharField(max_length=250)

    pool = models.ForeignKey('invoicing.Pool', on_delete=models.PROTECT, null=True)

    created_at = models.DateTimeField(auto_now_add=True, null=True)
    updated_at = models.DateTimeField(auto_now=True, null=True)

    class Meta:
        abstract = True

    def save(self, *args, **kwargs):
        shorten_slug(self)
        super().save(*args, **kwargs)

    @property
    def user_name(self):
        user_name = '%s %s' % (self.user_first_name, self.user_last_name)
        return user_name.strip()

    def display_description(self):
        if self.description == '@overtaking@':
            return False
        return True
