# lingo - payment and billing system
# Copyright (C) 2025  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import copy
import sys
import traceback
from itertools import islice

from django.conf import settings
from django.core.serializers.json import DjangoJSONEncoder
from django.db import connection, models, transaction
from django.utils.formats import date_format
from django.utils.timezone import now
from django.utils.translation import gettext_lazy as _

from lingo.agendas.chrono import ChronoError, lock_events_check
from lingo.agendas.models import Agenda
from lingo.invoicing import errors
from lingo.invoicing.models.base import DOCUMENT_MODELS
from lingo.pricing import errors as pricing_errors
from lingo.utils.fields import RichTextField
from lingo.utils.misc import shorten_slug


class Campaign(models.Model):
    label = models.CharField(_('Label'), max_length=150)
    regie = models.ForeignKey('invoicing.Regie', on_delete=models.PROTECT)
    date_start = models.DateField(_('Start date'))
    date_end = models.DateField(_('End date'))
    date_publication = models.DateField(_('Publication date'))
    date_payment_deadline_displayed = models.DateField(_('Displayed payment deadline'), null=True, blank=True)
    date_payment_deadline = models.DateField(_('Effective payment deadline'))
    date_due = models.DateField(_('Due date'))
    date_debit = models.DateField(_('Debit date'))
    injected_lines = models.CharField(
        _('Integrate injected lines'),
        choices=[
            ('no', _('no')),
            ('period', _('yes, only for the period')),
            ('all', _('yes, all injected lines before the end of the period')),
        ],
        default='no',
        max_length=10,
    )
    adjustment_campaign = models.BooleanField(_('Adjustment campaign'), default=False)
    agendas = models.ManyToManyField(Agenda, related_name='campaigns')
    invalid = models.BooleanField(default=False)
    finalized = models.BooleanField(default=False)

    invoice_model = models.CharField(
        _('Invoice model'),
        max_length=10,
        choices=DOCUMENT_MODELS,
        default='middle',
    )
    invoice_custom_text = RichTextField(
        _('Custom text in invoice'),
        blank=True,
        null=True,
        help_text=_('Displayed under the address and additional information blocks.'),
    )

    primary_campaign = models.ForeignKey(
        'self', null=True, on_delete=models.PROTECT, related_name='corrective_campaigns'
    )

    created_at = models.DateTimeField(auto_now_add=True, null=True)
    updated_at = models.DateTimeField(auto_now=True, null=True)

    def __str__(self):
        return _('%(label)s (%(start)s - %(end)s)') % {
            'label': self.label,
            'start': date_format(self.date_start, 'd/m/Y'),
            'end': date_format(self.date_end, 'd/m/Y'),
        }

    @property
    def is_last(self):
        if self.primary_campaign_id is None:
            if not Campaign.objects.filter(primary_campaign=self).exists():
                return True
            return False
        return not Campaign.objects.filter(
            primary_campaign=self.primary_campaign, created_at__gt=self.created_at
        ).exists()

    def mark_as_valid(self):
        self.invalid = False
        self.save()

    def mark_as_invalid(self, commit=True):
        self.invalid = True
        if commit:
            self.save()

    def mark_as_finalized(self):
        self.finalized = True
        self.save()
        self.assign_credits()

    def generate(self, spool=True):
        pool = self.pool_set.create(draft=True)
        try:
            pool.init()
        except Exception:
            return

        if spool and 'uwsgi' in sys.modules:
            from lingo.invoicing.spooler import generate_invoices

            tenant = getattr(connection, 'tenant', None)
            transaction.on_commit(
                lambda: generate_invoices.spool(
                    campaign_id=str(self.pk), pool_id=str(pool.pk), domain=getattr(tenant, 'domain_url', None)
                )
            )
            return

        pool.generate_invoices()

    def assign_credits(self, spool=True):
        if spool and 'uwsgi' in sys.modules:
            from lingo.invoicing.spooler import make_assignments

            tenant = getattr(connection, 'tenant', None)
            transaction.on_commit(
                lambda: make_assignments.spool(
                    campaign_id=str(self.pk), domain=getattr(tenant, 'domain_url', None)
                )
            )
            return

        self.make_assignments()

    def make_assignments(self):
        from lingo.invoicing.models import Credit, Invoice

        # assign generated credits to existing invoices
        invoices_qs = Invoice.objects.filter(
            pool__campaign=self,
            date_due__gte=now().date(),
            remaining_amount__gt=0,
        ).order_by('pk')
        for invoice in invoices_qs:
            invoice.make_assignments()
        # assign existing credits to generated invoices
        credits_qs = Credit.objects.filter(
            pool__campaign=self,
            remaining_amount__gt=0,
        ).order_by('pk')
        for credit in credits_qs:
            credit.make_assignments()

    def get_agenda_unlock_logs(self):
        if self.primary_campaign:
            return self.primary_campaign.get_agenda_unlock_logs()
        return self.agendaunlocklog_set.filter(agenda__in=self.agendas.all(), active=True).select_related(
            'agenda'
        )


class Pool(models.Model):
    campaign = models.ForeignKey(Campaign, on_delete=models.PROTECT)
    draft = models.BooleanField(default=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True, null=True)
    completed_at = models.DateTimeField(null=True)
    status = models.CharField(
        choices=[
            ('registered', _('Registered')),
            ('running', _('Running')),
            ('failed', _('Failed')),
            ('completed', _('Completed')),
        ],
        default='registered',
        max_length=100,
    )
    exception = models.TextField()

    @property
    def is_last(self):
        return not self.campaign.pool_set.filter(created_at__gt=self.created_at).exists()

    def init(self):
        from lingo.invoicing import utils

        try:
            agendas = utils.get_agendas(pool=self)
            if agendas:
                lock_events_check(
                    agenda_slugs=[a.slug for a in agendas],
                    date_start=self.campaign.date_start,
                    date_end=self.campaign.date_end,
                )
        except ChronoError as e:
            self.status = 'failed'
            self.exception = e.msg
            self.completed_at = now()
            self.save()
            raise
        except Exception:
            self.status = 'failed'
            self.exception = traceback.format_exc()
            self.completed_at = now()
            self.save()
            raise

    def generate_invoices(self):
        from lingo.invoicing import utils

        self.status = 'running'
        self.save()
        try:
            # get agendas with pricing corresponding to the period
            agendas = utils.get_agendas(pool=self)
            # get subscribed users for each agenda, for the period
            users = utils.get_users_from_subscriptions(agendas=agendas, pool=self)
            # get invoice lines for all subscribed users, for each agenda in the corresponding period
            lines = utils.get_all_lines(agendas=agendas, users=users, pool=self)
            # and generate invoices
            utils.generate_invoices_from_lines(all_lines=lines, pool=self)
        except ChronoError as e:
            self.status = 'failed'
            self.exception = e.msg
        except Exception:
            self.status = 'failed'
            self.exception = traceback.format_exc()
            raise
        finally:
            if self.status == 'running':
                self.status = 'completed'
            self.completed_at = now()
            self.save()

    def promote(self):
        if not self.is_last:
            # not the last
            raise errors.PoolPromotionError('Pool too old')
        if not self.draft:
            # not a draft
            raise errors.PoolPromotionError('Pool is final')
        if self.status != 'completed':
            # not completed
            raise errors.PoolPromotionError('Pool is not completed')

        final_pool = copy.deepcopy(self)
        final_pool.pk = None
        final_pool.draft = False
        final_pool.status = 'registered'
        final_pool.completed_at = None
        final_pool.save()

        if 'uwsgi' in sys.modules:
            from lingo.invoicing.spooler import populate_from_draft

            tenant = getattr(connection, 'tenant', None)
            transaction.on_commit(
                lambda: populate_from_draft.spool(
                    draft_pool_id=str(self.pk),
                    final_pool_id=str(final_pool.pk),
                    domain=getattr(tenant, 'domain_url', None),
                )
            )
            return

        final_pool.populate_from_draft(self)

    def populate_from_draft(self, draft_pool):
        try:
            self.status = 'running'
            self.save()

            batch_size = 1000

            # generate journal lines in the same order as drafts, by batch
            lines = draft_pool.draftjournalline_set.order_by('pk').iterator(chunk_size=batch_size)
            journal_line_mapping = {}
            while True:
                batch = list(islice(lines, batch_size))
                if not batch:
                    break
                final_lines = []
                for line in batch:
                    final_line = line.promote(pool=self, bulk=True)
                    final_lines.append(final_line)
                # bulk create journal lines
                JournalLine.objects.bulk_create(final_lines, batch_size)
                # keep a mapping draft line -> final line
                journal_line_mapping.update({jl._original_line.pk: jl.pk for jl in final_lines})

            # now create invoices and credits, and update journal lines to set invoice_line/credit_line FKs
            for invoice in draft_pool.draftinvoice_set.order_by('pk').iterator(chunk_size=batch_size):
                invoice.promote(pool=self, journal_line_mapping=journal_line_mapping)

        except Exception:
            self.status = 'failed'
            self.exception = traceback.format_exc()
            raise
        finally:
            if self.status == 'running':
                self.status = 'completed'
            self.completed_at = now()
            self.save()


class InjectedLine(models.Model):
    event_date = models.DateField()
    slug = models.SlugField(max_length=1000)
    label = models.TextField()
    amount = models.DecimalField(max_digits=9, decimal_places=2)

    user_external_id = models.CharField(max_length=250)
    payer_external_id = models.CharField(max_length=250)
    payer_first_name = models.CharField(max_length=250)
    payer_last_name = models.CharField(max_length=250)
    payer_address = models.TextField()
    payer_demat = models.BooleanField(default=False)
    payer_direct_debit = models.BooleanField(default=False)
    regie = models.ForeignKey('invoicing.Regie', on_delete=models.PROTECT)

    created_at = models.DateTimeField(auto_now_add=True, null=True)
    updated_at = models.DateTimeField(auto_now=True, null=True)

    def save(self, *args, **kwargs):
        shorten_slug(self)
        super().save(*args, **kwargs)


class AbstractJournalLine(models.Model):
    event_date = models.DateField()
    slug = models.SlugField(max_length=1000)
    label = models.TextField()
    description = models.TextField()
    amount = models.DecimalField(max_digits=9, decimal_places=2)
    quantity = models.IntegerField(default=1)
    quantity_type = models.CharField(
        max_length=10,
        choices=[
            ('units', _('Units')),
            ('minutes', _('Minutes')),
        ],
        default='units',
    )
    accounting_code = models.CharField(max_length=250, blank=True)

    user_external_id = models.CharField(max_length=250)
    user_first_name = models.CharField(max_length=250)
    user_last_name = models.CharField(max_length=250)
    payer_external_id = models.CharField(max_length=250)
    payer_first_name = models.CharField(max_length=250)
    payer_last_name = models.CharField(max_length=250)
    payer_address = models.TextField()
    payer_demat = models.BooleanField(default=False)
    payer_direct_debit = models.BooleanField(default=False)
    event = models.JSONField(default=dict)
    booking = models.JSONField(default=dict)
    pricing_data = models.JSONField(default=dict, encoder=DjangoJSONEncoder)
    status = models.CharField(
        max_length=10,
        choices=[
            ('success', _('Success')),
            ('warning', _('Warning')),
            ('error', _('Error')),
        ],
    )
    error_status = models.CharField(
        max_length=10,
        choices=[
            ('ignored', _('Ignored')),
            ('fixed', _('Fixed')),
        ],
        blank=True,
    )

    pool = models.ForeignKey(Pool, on_delete=models.PROTECT, null=True)
    from_injected_line = models.ForeignKey(InjectedLine, on_delete=models.PROTECT, null=True)

    created_at = models.DateTimeField(auto_now_add=True, null=True)
    updated_at = models.DateTimeField(auto_now=True, null=True)

    class Meta:
        abstract = True

    @property
    def user_name(self):
        user_name = '%s %s' % (self.user_first_name, self.user_last_name)
        return user_name.strip()

    @property
    def payer_name(self):
        payer_name = '%s %s' % (self.payer_first_name, self.payer_last_name)
        return payer_name.strip()

    @staticmethod
    def get_error_class(error_type):
        error_classes = {
            'PricingNotFound': pricing_errors.PricingNotFound,
            'CriteriaConditionNotFound': pricing_errors.CriteriaConditionNotFound,
            'MultipleDefaultCriteriaCondition': pricing_errors.MultipleDefaultCriteriaCondition,
            'PricingDataError': pricing_errors.PricingDataError,
            'PricingDataFormatError': pricing_errors.PricingDataFormatError,
            'MinPricingDataError': pricing_errors.MinPricingDataError,
            'MinPricingDataFormatError': pricing_errors.MinPricingDataFormatError,
            'PricingReductionRateError': pricing_errors.PricingReductionRateError,
            'PricingReductionRateFormatError': pricing_errors.PricingReductionRateFormatError,
            'PricingReductionRateValueError': pricing_errors.PricingReductionRateValueError,
            'PricingEffortRateTargetError': pricing_errors.PricingEffortRateTargetError,
            'PricingEffortRateTargetFormatError': pricing_errors.PricingEffortRateTargetFormatError,
            'PricingEffortRateTargetValueError': pricing_errors.PricingEffortRateTargetValueError,
            'PricingAccountingCodeError': pricing_errors.PricingAccountingCodeError,
            'PricingUnknownCheckStatusError': pricing_errors.PricingUnknownCheckStatusError,
            'PricingEventNotCheckedError': pricing_errors.PricingEventNotCheckedError,
            'PricingBookingNotCheckedError': pricing_errors.PricingBookingNotCheckedError,
            'PricingMultipleBookingError': pricing_errors.PricingMultipleBookingError,
            'PricingBookingCheckTypeError': pricing_errors.PricingBookingCheckTypeError,
            'PayerError': errors.PayerError,
            'PayerDataError': errors.PayerDataError,
        }
        return error_classes.get(error_type)

    @staticmethod
    def get_error_label(error_type):
        error_class = AbstractJournalLine.get_error_class(error_type)
        if error_class is None:
            return error_type
        return error_class.label

    def get_error_display(self):
        if self.status == 'success':
            return
        error = str(self.pricing_data.get('error'))
        error_class = AbstractJournalLine.get_error_class(error)
        if error_class is None:
            return error
        error_details = self.pricing_data.get('error_details', {})
        error = error_class(details=error_details)
        return error.get_error_display()

    def get_chrono_event_url(self):
        if not settings.KNOWN_SERVICES.get('chrono'):
            return
        chrono = list(settings.KNOWN_SERVICES['chrono'].values())[0]
        chrono_url = chrono.get('url')
        if not chrono_url:
            return
        if not self.event.get('agenda') or not self.event.get('slug'):
            return
        return '%smanage/agendas/%s/events/%s/' % (chrono_url, self.event['agenda'], self.event['slug'])

    def save(self, *args, **kwargs):
        shorten_slug(self)
        super().save(*args, **kwargs)


class DraftJournalLine(AbstractJournalLine):
    invoice_line = models.ForeignKey(
        'invoicing.DraftInvoiceLine', on_delete=models.PROTECT, null=True, related_name='journal_lines'
    )

    class Meta:
        indexes = [
            models.Index(fields=['pool', 'status']),
            models.Index(
                fields=['pool_id'],
                condition=models.Q(pricing_data__error__isnull=False),
                name='invoicing_djl_error_idx',
            ),
        ]

    def promote(self, pool=None, invoice_line=None, credit_line=None, bulk=False):
        final_line = copy.deepcopy(self)
        final_line.__class__ = JournalLine
        final_line.pk = None
        final_line.pool = pool
        final_line.invoice_line = invoice_line
        final_line.credit_line = credit_line
        final_line.error_status = ''
        if not bulk:
            final_line.save()
        final_line._original_line = self
        return final_line


class JournalLine(AbstractJournalLine):
    invoice_line = models.ForeignKey(
        'invoicing.InvoiceLine', on_delete=models.PROTECT, null=True, related_name='journal_lines'
    )
    credit_line = models.ForeignKey(
        'invoicing.CreditLine', on_delete=models.PROTECT, null=True, related_name='journal_lines'
    )

    class Meta:
        indexes = [
            models.Index(fields=['pool', 'status', 'error_status']),
            models.Index(
                fields=['pool_id'],
                condition=models.Q(pricing_data__error__isnull=False),
                name='invoicing_jl_error_idx',
            ),
        ]
