# lingo - payment and billing system
# Copyright (C) 2023  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.shortcuts import get_object_or_404
from django.utils.translation import gettext_noop as N_
from rest_framework import permissions
from rest_framework.views import APIView

from lingo.agendas.models import Agenda, AgendaUnlockLog
from lingo.api import serializers
from lingo.api.utils import APIErrorBadRequest, Response
from lingo.invoicing.models import Campaign


class AgendaCheckTypeList(APIView):
    permission_classes = ()

    def get(self, request, agenda_identifier=None, format=None):
        agenda = get_object_or_404(Agenda, slug=agenda_identifier)

        check_types = []
        if agenda.check_type_group:
            check_types = [
                {
                    'id': x.slug,
                    'text': x.label,
                    'kind': x.kind,
                    'code': x.code,
                    'unexpected_presence': bool(agenda.check_type_group.unexpected_presence == x),
                    'unjustified_absence': bool(agenda.check_type_group.unjustified_absence == x),
                }
                for x in agenda.check_type_group.check_types.filter(disabled=False)
            ]
        return Response({'data': check_types})


agenda_check_type_list = AgendaCheckTypeList.as_view()


class AgendaUnlock(APIView):
    permission_classes = (permissions.IsAuthenticated,)
    serializer_class = serializers.AgendaUnlockSerializer

    def post(self, request):
        serializer = self.serializer_class(data=request.data)
        if not serializer.is_valid():
            raise APIErrorBadRequest(N_('invalid payload'), errors=serializer.errors)

        campaign_qs = Campaign.objects.filter(
            date_start__lt=serializer.validated_data['date_end'],
            date_end__gt=serializer.validated_data['date_start'],
            # exclude corrective campaigns
            primary_campaign__isnull=True,
        )

        for agenda in serializer.validated_data['agendas']:
            for campaign in campaign_qs.filter(agendas=agenda):
                last_log = (
                    AgendaUnlockLog.objects.filter(agenda=agenda, campaign=campaign)
                    .order_by('created_at')
                    .last()
                )
                if last_log and last_log.active:
                    # update updated_at field
                    last_log.save()
                    continue
                AgendaUnlockLog.objects.create(agenda=agenda, campaign=campaign)

        return Response({'err': 0})


agenda_unlock = AgendaUnlock.as_view()
