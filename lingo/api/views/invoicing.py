# lingo - payment and billing system
# Copyright (C) 2023  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import datetime
import decimal
import logging

import pytz
from django.core.exceptions import ObjectDoesNotExist
from django.db import transaction
from django.db.models import Q
from django.http import Http404, HttpResponse
from django.shortcuts import get_object_or_404
from django.template.defaultfilters import floatformat
from django.urls import reverse
from django.utils.timezone import now
from django.utils.translation import gettext_lazy as _
from django.utils.translation import gettext_noop as N_
from rest_framework import permissions
from rest_framework.views import APIView
from weasyprint import HTML

from lingo.api import serializers
from lingo.api.utils import APIErrorBadRequest, Response
from lingo.invoicing.errors import PayerError
from lingo.invoicing.models import (
    PAYMENT_INFO,
    Credit,
    CreditAssignment,
    DraftInvoice,
    DraftInvoiceLine,
    InjectedLine,
    Invoice,
    InvoiceCancellationReason,
    InvoiceLine,
    Payment,
    PaymentType,
    Refund,
    Regie,
)


class InvoiceCancellationReasons(APIView):
    permission_classes = (permissions.IsAuthenticated,)

    def get(self, request):
        return Response(
            {
                'data': [
                    {'id': reason.slug, 'text': reason.label, 'slug': reason.slug}
                    for reason in InvoiceCancellationReason.objects.filter(disabled=False)
                ]
            }
        )


invoice_cancellation_reasons = InvoiceCancellationReasons.as_view()


class InvoicingRegies(APIView):
    permission_classes = (permissions.IsAuthenticated,)

    def get(self, request, format=None):
        return Response(
            {
                'data': [
                    {'id': regie.slug, 'text': regie.label, 'slug': regie.slug}
                    for regie in Regie.objects.order_by('label')
                ]
            }
        )


invoicing_regies = InvoicingRegies.as_view()


class InvoicingPaymentTypes(APIView):
    permission_classes = (permissions.IsAuthenticated,)

    def get(self, request, regie_identifier):
        regie = get_object_or_404(Regie, slug=regie_identifier)
        return Response(
            {
                'data': [
                    {'id': payment_type.slug, 'text': payment_type.label, 'slug': payment_type.slug}
                    for payment_type in PaymentType.objects.filter(regie=regie, disabled=False).exclude(
                        slug='credit'
                    )
                ]
            }
        )


invoicing_payment_types = InvoicingPaymentTypes.as_view()


class PayerMixin:
    def get_payer_external_id(self, request, regie, nameid=None, payer_external_id=None):
        if payer_external_id:
            return payer_external_id
        if not nameid:
            raise Http404
        try:
            return regie.get_payer_external_id_from_nameid(request, nameid)
        except PayerError:
            raise Http404


class InvoicingInvoices(PayerMixin, APIView):
    permission_classes = (permissions.IsAuthenticated,)
    serializer_class = serializers.InvoiceFiltersSerializer
    invoice_always_enabled = False

    def get_invoices_queryset(self, request, regie):
        serializer = self.serializer_class(data=request.query_params)
        if not serializer.is_valid():
            raise APIErrorBadRequest(N_('invalid filters'), errors=serializer.errors)
        data = serializer.validated_data
        for_backoffice = bool(request.GET.get('payer_external_id'))

        payer_external_id = self.get_payer_external_id(
            request=request,
            regie=regie,
            nameid=request.GET.get('NameID'),
            payer_external_id=request.GET.get('payer_external_id'),
        )
        queryset = Invoice.objects.filter(
            regie=regie,
            remaining_amount__gt=0,
            date_publication__lte=now().date(),
            payer_external_id=payer_external_id,
            basket__isnull=True,
            cancelled_at__isnull=True,
            collection__isnull=True,
        ).exclude(pool__campaign__finalized=False)

        if 'payable' in data and 'payable' in request.query_params:
            date_field = 'date_due' if for_backoffice else 'date_payment_deadline'
            if data['payable'] is False:
                qs_qargs = Q(**{'%s__lt' % date_field: now().date()}) | Q(payer_direct_debit=True)
            else:
                qs_qargs = Q(**{'%s__gte' % date_field: now().date()}) & Q(payer_direct_debit=False)
            queryset = queryset.filter(qs_qargs)

        return queryset.order_by('-created_at')

    def get(self, request, regie_identifier):
        regie = get_object_or_404(Regie, slug=regie_identifier)
        try:
            include_epayment_url = bool(regie.paymentbackend)
        except ObjectDoesNotExist:
            include_epayment_url = False
        invoices = self.get_invoices_queryset(request, regie)
        include_lines = request.GET.get('include_lines')
        label_plus = request.GET.get('verbose_label')
        data = []
        for invoice in invoices:
            invoice_data = invoice.normalize(
                for_backoffice=bool(request.GET.get('payer_external_id')),
                label_plus=(include_lines or label_plus),
                always_enabled=self.invoice_always_enabled,
            )
            if include_epayment_url and invoice_data['online_payment']:
                invoice_data['api'] = {
                    'payment_url': request.build_absolute_uri(
                        reverse('lingo-epayment-invoice', kwargs={'invoice_uuid': invoice.uuid})
                    )
                }
            data.append(invoice_data)
            if bool(include_lines) and not invoice_data['disabled']:
                lines = invoice.get_grouped_and_ordered_lines()
                for line in lines:
                    amount = _('%(amount)s€') % {'amount': floatformat(line.remaining_amount, 2)}
                    parts = ['----']
                    if line.activity_label:
                        parts.append('%s -' % line.activity_label)
                    parts.append(line.label)
                    if line.details.get('check_type_label'):
                        parts.append('- %s' % line.details['check_type_label'])
                    elif line.details.get('status') == 'absence':
                        parts.append('- %s' % _('Absence'))
                    if line.display_description() and line.description:
                        parts.append('- %s' % line.description)
                    parts.append(_('(amount to pay: %s)') % amount)
                    data.append(
                        {
                            'id': 'line:%s' % line.uuid,
                            'label': ' '.join(parts),
                            'disabled': not bool(line.remaining_amount),
                            'is_line': True,
                            'activity_label': line.activity_label,
                            'agenda_slug': line.agenda_slug,
                            'user_external_id': line.user_external_id,
                            'user_first_name': line.user_first_name,
                            'user_last_name': line.user_last_name,
                            'event_date': line.event_date,
                            'details': line.details,
                            'line_label': line.label,
                            'line_description': line.description if line.display_description() else '',
                            'line_raw_description': line.description,
                            'line_slug': line.slug,
                            'accounting_code': line.accounting_code,
                            'unit_amount': line.unit_amount,
                            'quantity': line.quantity,
                            'remaining_amount': line.remaining_amount,
                            'invoice_id': line.invoice.uuid,
                        }
                    )
        return Response({'data': data})


invoicing_invoices = InvoicingInvoices.as_view()
invoicing_invoices.publik_authentication_resolve_user_by_nameid = False


class InvoicingHistoryInvoices(InvoicingInvoices):
    def get_invoices_queryset(self, request, regie):
        payer_external_id = self.get_payer_external_id(
            request=request,
            regie=regie,
            nameid=request.GET.get('NameID'),
            payer_external_id=request.GET.get('payer_external_id'),
        )
        return (
            Invoice.objects.filter(
                regie=regie,
                remaining_amount=0,
                date_publication__lte=now().date(),
                payer_external_id=payer_external_id,
                cancelled_at__isnull=True,
                collection__isnull=True,
            )
            .exclude(pool__campaign__finalized=False)
            .order_by('-created_at')
        )


invoicing_history_invoices = InvoicingHistoryInvoices.as_view()
invoicing_history_invoices.publik_authentication_resolve_user_by_nameid = False


class InvoicingCollectedInvoices(InvoicingInvoices):
    def get_invoices_queryset(self, request, regie):
        payer_external_id = self.get_payer_external_id(
            request=request,
            regie=regie,
            nameid=request.GET.get('NameID'),
            payer_external_id=request.GET.get('payer_external_id'),
        )
        return Invoice.objects.filter(
            Q(pool__isnull=True) | Q(pool__campaign__finalized=True),
            regie=regie,
            date_publication__lte=now().date(),
            payer_external_id=payer_external_id,
            cancelled_at__isnull=True,
            collection__isnull=False,
            collection__draft=False,
        ).order_by('-created_at')


invoicing_collected_invoices = InvoicingCollectedInvoices.as_view()
invoicing_collected_invoices.publik_authentication_resolve_user_by_nameid = False


class InvoicingCancelledInvoices(InvoicingInvoices):
    invoice_always_enabled = True

    def get_invoices_queryset(self, request, regie):
        if not request.GET.get('payer_external_id'):
            raise Http404
        payer_external_id = request.GET['payer_external_id']
        return (
            Invoice.objects.filter(
                regie=regie,
                payer_external_id=payer_external_id,
                basket__isnull=True,
                cancelled_at__isnull=False,
                collection__isnull=True,
            )
            .exclude(pool__campaign__finalized=False)
            .order_by('-created_at')
        )


invoicing_cancelled_invoices = InvoicingCancelledInvoices.as_view()


class InvoicingInvoice(PayerMixin, APIView):
    permission_classes = (permissions.IsAuthenticated,)

    def get(self, request, regie_identifier, invoice_identifier):
        regie = get_object_or_404(Regie, slug=regie_identifier)
        payer_external_id = self.get_payer_external_id(
            request=request,
            regie=regie,
            nameid=request.GET.get('NameID'),
            payer_external_id=request.GET.get('payer_external_id'),
        )
        invoice = get_object_or_404(
            Invoice.objects.exclude(pool__campaign__finalized=False),
            uuid=invoice_identifier,
            regie=regie,
            date_publication__lte=now().date(),
            payer_external_id=payer_external_id,
            cancelled_at__isnull=True,
        )
        invoice_data = invoice.normalize(for_backoffice=bool(request.GET.get('payer_external_id')))
        try:
            if bool(regie.paymentbackend) and invoice_data['online_payment']:
                invoice_data['api'] = {
                    'payment_url': request.build_absolute_uri(
                        reverse('lingo-epayment-invoice', kwargs={'invoice_uuid': invoice.uuid})
                    )
                }
        except ObjectDoesNotExist:
            pass

        return Response({'data': invoice_data})


invoicing_invoice = InvoicingInvoice.as_view()
invoicing_invoice.publik_authentication_resolve_user_by_nameid = False


class InvoicingInvoicePDF(PayerMixin, APIView):
    permission_classes = (permissions.IsAuthenticated,)
    dynamic = False

    def get(self, request, regie_identifier, invoice_identifier):
        regie = get_object_or_404(Regie, slug=regie_identifier)
        payer_external_id = self.get_payer_external_id(
            request=request,
            regie=regie,
            nameid=request.GET.get('NameID'),
            payer_external_id=request.GET.get('payer_external_id'),
        )
        invoice = get_object_or_404(
            Invoice.objects.exclude(pool__campaign__finalized=False),
            uuid=invoice_identifier,
            regie=regie,
            date_publication__lte=now().date(),
            payer_external_id=payer_external_id,
            cancelled_at__isnull=True,
        )
        result = invoice.html(dynamic=self.dynamic)
        html = HTML(string=result)
        pdf = html.write_pdf()
        response = HttpResponse(pdf, content_type='application/pdf')
        response['Content-Disposition'] = 'attachment; filename="%s.pdf"' % invoice.formatted_number
        return response


invoicing_invoice_pdf = InvoicingInvoicePDF.as_view()
invoicing_invoice_pdf.publik_authentication_resolve_user_by_nameid = False


class InvoicingInvoiceDynamicPDF(InvoicingInvoicePDF):
    dynamic = True


invoicing_invoice_dynamic_pdf = InvoicingInvoiceDynamicPDF.as_view()
invoicing_invoice_dynamic_pdf.publik_authentication_resolve_user_by_nameid = False


class InvoicingInvoicePaymentsPDF(PayerMixin, APIView):
    permission_classes = (permissions.IsAuthenticated,)

    def get(self, request, regie_identifier, invoice_identifier):
        regie = get_object_or_404(Regie, slug=regie_identifier)
        payer_external_id = self.get_payer_external_id(
            request=request,
            regie=regie,
            nameid=request.GET.get('NameID'),
            payer_external_id=request.GET.get('payer_external_id'),
        )
        invoice = get_object_or_404(
            Invoice.objects.exclude(pool__campaign__finalized=False),
            uuid=invoice_identifier,
            regie=regie,
            date_publication__lte=now().date(),
            payer_external_id=payer_external_id,
            remaining_amount=0,
            cancelled_at__isnull=True,
            collection__isnull=True,
        )
        result = invoice.payments_html()
        html = HTML(string=result)
        pdf = html.write_pdf()
        response = HttpResponse(pdf, content_type='application/pdf')
        response['Content-Disposition'] = 'attachment; filename="A-%s.pdf"' % invoice.formatted_number
        return response


invoicing_invoice_payments_pdf = InvoicingInvoicePaymentsPDF.as_view()
invoicing_invoice_payments_pdf.publik_authentication_resolve_user_by_nameid = False


class InvoicingInvoicePay(APIView):
    # XXX ?
    authentication_classes = []
    permission_classes = ()

    def post(self, request, regie_identifier, invoice_identifier):
        logging.error(
            'Deprecated enpoint /api/regie/<regie_identifier>/invoice/<invoice_identifier>/pay/ called'
        )
        data = request.data
        transaction_id = data.get('transaction_id')
        transaction_date = data.get('transaction_date')
        if transaction_date:
            try:
                transaction_date = pytz.utc.localize(
                    datetime.datetime.strptime(transaction_date, '%Y-%m-%dT%H:%M:%S')
                )
            except ValueError:
                transaction_date = None
        order_id = data.get('order_id')
        bank_transaction_id = data.get('bank_transaction_id')
        bank_transaction_date = data.get('bank_transaction_date')
        if bank_transaction_date:
            try:
                bank_transaction_date = pytz.utc.localize(
                    datetime.datetime.strptime(bank_transaction_date, '%Y-%m-%dT%H:%M:%S')
                )
            except ValueError:
                bank_transaction_date = None
        bank_data = data.get('bank_data')
        invoice = get_object_or_404(
            Invoice.objects.exclude(pool__campaign__finalized=False),
            uuid=invoice_identifier,
            regie__slug=regie_identifier,
            remaining_amount__gt=0,
            cancelled_at__isnull=True,
            collection__isnull=True,
        )
        try:
            amount = decimal.Decimal(str(data.get('amount')))
        except (decimal.InvalidOperation, ValueError, TypeError):
            amount = invoice.remaining_amount
        payment_type = get_object_or_404(PaymentType, regie=invoice.regie, slug='online')
        payment = Payment.make_payment(
            regie=invoice.regie,
            invoices=[invoice],
            amount=amount,
            payment_type=payment_type,
            transaction_id=transaction_id,
            transaction_date=transaction_date,
            order_id=order_id,
            bank_transaction_id=bank_transaction_id,
            bank_transaction_date=bank_transaction_date,
            bank_data=bank_data,
        )
        return Response(
            {
                'data': {
                    'id': payment.uuid,
                }
            }
        )


invoicing_invoice_pay = InvoicingInvoicePay.as_view()


class InvoicingInvoiceCancel(APIView):
    permission_classes = (permissions.IsAuthenticated,)
    serializer_class = serializers.CancelInvoiceSerializer

    def post(self, request, regie_identifier, invoice_identifier):
        regie = get_object_or_404(Regie, slug=regie_identifier)
        invoice = get_object_or_404(
            Invoice.objects.exclude(
                pk__in=InvoiceLine.objects.filter(invoicelinepayment__isnull=False).values('invoice')
            ).exclude(pool__campaign__finalized=False),
            uuid=invoice_identifier,
            regie=regie,
            cancelled_at__isnull=True,
            collection__isnull=True,
        )

        serializer = self.serializer_class(data=request.data)
        if not serializer.is_valid():
            raise APIErrorBadRequest(N_('invalid payload'), errors=serializer.errors)

        invoice.cancelled_at = now()
        invoice.cancellation_reason = serializer.validated_data['cancellation_reason']
        invoice.cancellation_description = serializer.validated_data.get('cancellation_description') or ''
        invoice.cancelled_by = serializer.validated_data.get('user_uuid') or None
        invoice.save()
        if serializer.validated_data['notify'] is True:
            invoice.notify(payload={'invoice_id': str(invoice.uuid)}, notification_type='cancel')
        return Response({'err': 0})


invoicing_invoice_cancel = InvoicingInvoiceCancel.as_view()


class InvoicingDraftInvoices(APIView):
    permission_classes = (permissions.IsAuthenticated,)
    serializer_class = serializers.DraftInvoiceSerializer

    def post(self, request, regie_identifier):
        regie = get_object_or_404(Regie, slug=regie_identifier)
        serializer = self.serializer_class(data=request.data, regie=regie)
        if not serializer.is_valid():
            raise APIErrorBadRequest(N_('invalid payload'), errors=serializer.errors)

        payer_demat = serializer.validated_data.pop('payer_demat', False)
        date_due = serializer.validated_data.pop('date_due', serializer.validated_data['date_publication'])
        date_payment_deadline = serializer.validated_data.pop(
            'date_payment_deadline', serializer.validated_data['date_publication']
        )
        invoice = DraftInvoice.objects.create(
            regie=regie,
            payer_demat=payer_demat,
            date_due=date_due,
            date_payment_deadline=date_payment_deadline,
            **serializer.validated_data,
        )

        return Response({'data': {'draft_invoice_id': str(invoice.uuid)}})


invoicing_draft_invoices = InvoicingDraftInvoices.as_view()


class InvoicingDraftCredits(InvoicingDraftInvoices):
    serializer_class = serializers.DraftCreditSerializer


invoicing_draft_credits = InvoicingDraftCredits.as_view()


class InvoicingDraftInvoiceLines(APIView):
    permission_classes = (permissions.IsAuthenticated,)
    serializer_class = serializers.DraftInvoiceLineSerializer

    def post(self, request, regie_identifier, draft_identifier):
        regie = get_object_or_404(Regie, slug=regie_identifier)
        invoice = get_object_or_404(DraftInvoice, regie=regie, uuid=draft_identifier, pool__isnull=True)
        serializer = self.serializer_class(data=request.data)
        if not serializer.is_valid():
            raise APIErrorBadRequest(N_('invalid payload'), errors=serializer.errors)

        event_slug = serializer.validated_data['slug']
        event_label = ''
        agenda_slug = ''
        if '@' in event_slug:
            agenda_slug = event_slug.split('@')[0]
            event_label = serializer.validated_data['label']
        merge_lines = serializer.validated_data.pop('merge_lines')
        subject = serializer.validated_data.pop('subject')
        description = serializer.validated_data['description']

        values = {
            'invoice': invoice,
            'payer_external_id': invoice.payer_external_id,
            'payer_first_name': invoice.payer_first_name,
            'payer_last_name': invoice.payer_last_name,
            'payer_address': invoice.payer_address,
            'payer_demat': invoice.payer_demat,
            'payer_direct_debit': invoice.payer_direct_debit,
            'event_slug': event_slug,
            'event_label': event_label,
            'agenda_slug': agenda_slug,
            'details': {'dates': [serializer.validated_data['event_date'].isoformat()]},
        }
        values.update(**serializer.validated_data)
        if subject:
            values['description'] = f'{subject} {description}'

        if merge_lines is True and event_slug and agenda_slug and subject:
            line, created = DraftInvoiceLine.objects.filter(description__startswith=subject).get_or_create(
                invoice=invoice,
                label=serializer.validated_data['label'],
                event_slug=event_slug,
                event_label=event_label,
                agenda_slug=agenda_slug,
                activity_label=serializer.validated_data.get('activity_label') or '',
                unit_amount=serializer.validated_data['unit_amount'],
                accounting_code=serializer.validated_data.get('accounting_code') or '',
                user_external_id=serializer.validated_data['user_external_id'],
                form_url=serializer.validated_data.get('form_url') or '',
                defaults=values,
            )
            if not created:
                # existing line, update quantity and complete description
                line.quantity += serializer.validated_data['quantity']
                if description:
                    parts = []
                    if line.description:
                        parts.append(line.description)
                    parts.append(description)
                    line.description = ', '.join(parts)
                line.details['dates'].append(serializer.validated_data['event_date'].isoformat())
                line.save()
        else:
            line = DraftInvoiceLine.objects.create(**values)

        return Response({'data': {'draft_line_id': line.pk}})


invoicing_draft_invoice_lines = InvoicingDraftInvoiceLines.as_view()


class InvoicingDraftCreditLines(InvoicingDraftInvoiceLines):
    pass


invoicing_draft_credit_lines = InvoicingDraftCreditLines.as_view()


class InvoicingDraftInvoiceClose(APIView):
    permission_classes = (permissions.IsAuthenticated,)

    def post(self, request, regie_identifier, draft_identifier):
        regie = get_object_or_404(Regie, slug=regie_identifier)
        invoice = get_object_or_404(DraftInvoice, regie=regie, uuid=draft_identifier, pool__isnull=True)
        self.check_object(invoice)
        final_object = invoice.promote()
        final_object.make_assignments()
        return self.render_response(request, final_object)

    def check_object(self, obj):
        if obj.total_amount < 0:
            raise APIErrorBadRequest(N_('can not create invoice from draft invoice with negative amount'))

    def render_response(self, request, final_object):
        return Response({'data': final_object.get_notification_payload(request)})


invoicing_draft_invoice_close = InvoicingDraftInvoiceClose.as_view()


class InvoicingDraftCreditClose(InvoicingDraftInvoiceClose):
    def check_object(self, obj):
        if obj.total_amount >= 0:
            raise APIErrorBadRequest(N_('can not create credit from draft invoice with positive amount'))


invoicing_draft_credit_close = InvoicingDraftCreditClose.as_view()


class InjectedLines(APIView):
    permission_classes = (permissions.IsAuthenticated,)
    serializer_class = serializers.InjectedLineSerializer

    def post(self, request, regie_identifier):
        regie = get_object_or_404(Regie, slug=regie_identifier)
        serializer = self.serializer_class(data=request.data)
        if not serializer.is_valid():
            raise APIErrorBadRequest(N_('invalid payload'), errors=serializer.errors)

        instance = InjectedLine.objects.create(regie=regie, **serializer.validated_data)
        return Response({'err': 0, 'id': instance.pk})


injected_lines = InjectedLines.as_view()


class InvoicingPayments(PayerMixin, APIView):
    permission_classes = (permissions.IsAuthenticated,)
    serializer_class = serializers.MakePaymentSerializer

    def get(self, request, regie_identifier):
        regie = get_object_or_404(Regie, slug=regie_identifier)
        payer_external_id = self.get_payer_external_id(
            request=request,
            regie=regie,
            nameid=request.GET.get('NameID'),
            payer_external_id=request.GET.get('payer_external_id'),
        )
        payment_qs = Payment.objects
        if request.GET.get('NameID'):
            payment_qs = payment_qs.exclude(payment_type__slug='collect')
        payments = payment_qs.filter(
            regie=regie,
            payer_external_id=payer_external_id,
            cancelled_at__isnull=True,
        ).order_by('-created_at')
        return Response(
            {
                'data': [
                    {
                        'id': str(payment.uuid),
                        'display_id': payment.formatted_number,
                        'payment_type': payment.payment_type.label,
                        'amount': payment.amount,
                        'created': payment.created_at.date(),
                        'has_pdf': True,
                    }
                    for payment in payments
                ]
            }
        )

    def post(self, request, regie_identifier):
        regie = get_object_or_404(Regie, slug=regie_identifier)
        serializer = self.serializer_class(data=request.data, regie=regie)
        if not serializer.is_valid():
            raise APIErrorBadRequest(N_('invalid payload'), errors=serializer.errors)

        instance = Payment.make_payment(
            regie=regie,
            invoices=serializer._invoices,
            lines=serializer._lines,
            amount=serializer.validated_data['amount'],
            payment_type=serializer.validated_data['payment_type'],
            payment_info={
                k: serializer.validated_data[k] for k, l in PAYMENT_INFO if serializer.validated_data.get(k)
            },
            date_payment=serializer.validated_data.get('date_payment'),
        )
        return Response(
            {
                'err': 0,
                'id': str(instance.uuid),  # legacy
                'data': instance.get_notification_payload(request),
            }
        )


invoicing_payments = InvoicingPayments.as_view()
invoicing_payments.publik_authentication_resolve_user_by_nameid = False


class InvoicingPayment(PayerMixin, APIView):
    permission_classes = (permissions.IsAuthenticated,)
    serializer_class = serializers.PaymentSerializer

    def patch(self, request, regie_identifier, payment_identifier):
        regie = get_object_or_404(Regie, slug=regie_identifier)
        payment = get_object_or_404(
            Payment,
            uuid=payment_identifier,
            regie=regie,
        )
        serializer = self.serializer_class(payment, data=request.data, partial=True)
        if not serializer.is_valid():
            raise APIErrorBadRequest(N_('invalid payload'), errors=serializer.errors)

        for k, dummy in PAYMENT_INFO:
            if serializer.validated_data.get(k):
                payment.payment_info[k] = serializer.validated_data[k]
        payment.save()
        return Response(
            {
                'err': 0,
                'id': str(payment.uuid),  # legacy
                'data': payment.get_notification_payload(request),
            }
        )


invoicing_payment = InvoicingPayment.as_view()
invoicing_payment.publik_authentication_resolve_user_by_nameid = False


class InvoicingPaymentPDF(PayerMixin, APIView):
    permission_classes = (permissions.IsAuthenticated,)

    def get(self, request, regie_identifier, payment_identifier):
        regie = get_object_or_404(Regie, slug=regie_identifier)
        payer_external_id = self.get_payer_external_id(
            request=request,
            regie=regie,
            nameid=request.GET.get('NameID'),
            payer_external_id=request.GET.get('payer_external_id'),
        )
        payment_qs = Payment
        if request.GET.get('NameID'):
            payment_qs = Payment.objects.exclude(payment_type__slug='collect')
        payment = get_object_or_404(
            payment_qs,
            uuid=payment_identifier,
            regie=regie,
            payer_external_id=payer_external_id,
            cancelled_at__isnull=True,
        )
        result = payment.html()
        html = HTML(string=result)
        pdf = html.write_pdf()
        response = HttpResponse(pdf, content_type='application/pdf')
        response['Content-Disposition'] = 'attachment; filename="%s.pdf"' % payment.formatted_number
        return response


invoicing_payment_pdf = InvoicingPaymentPDF.as_view()
invoicing_payment_pdf.publik_authentication_resolve_user_by_nameid = False


class InvoicingCredits(PayerMixin, APIView):
    permission_classes = (permissions.IsAuthenticated,)

    def get_credits_queryset(self, request, regie):
        payer_external_id = self.get_payer_external_id(
            request=request,
            regie=regie,
            nameid=request.GET.get('NameID'),
            payer_external_id=request.GET.get('payer_external_id'),
        )
        return (
            Credit.objects.filter(
                regie=regie,
                remaining_amount__gt=0,
                payer_external_id=payer_external_id,
                date_publication__lte=now().date(),
                cancelled_at__isnull=True,
            )
            .exclude(pool__campaign__finalized=False)
            .order_by('-created_at')
        )

    def get(self, request, regie_identifier):
        regie = get_object_or_404(Regie, slug=regie_identifier)
        credits_qs = self.get_credits_queryset(request, regie)
        data = []
        for credit in credits_qs:
            label = '%s - %s' % (credit.formatted_number, credit.label)
            if credit.remaining_amount:
                amount = _('%(amount)s€') % {'amount': floatformat(credit.remaining_amount, 2)}
                label += ' ' + _('(credit left: %s)') % amount
            data.append(
                {
                    'id': str(credit.uuid),
                    'label': label,
                    'display_id': credit.formatted_number,
                    'remaining_amount': credit.remaining_amount,
                    'total_amount': credit.total_amount,
                    'created': credit.created_at.date(),
                    'usable': credit.usable,
                    'has_pdf': True,
                }
            )
        return Response({'data': data})


invoicing_credits = InvoicingCredits.as_view()
invoicing_credits.publik_authentication_resolve_user_by_nameid = False


class InvoicingHistoryCredits(InvoicingCredits):
    def get_credits_queryset(self, request, regie):
        payer_external_id = self.get_payer_external_id(
            request=request,
            regie=regie,
            nameid=request.GET.get('NameID'),
            payer_external_id=request.GET.get('payer_external_id'),
        )
        return (
            Credit.objects.filter(
                regie=regie,
                remaining_amount=0,
                payer_external_id=payer_external_id,
                date_publication__lte=now().date(),
                cancelled_at__isnull=True,
            )
            .exclude(pool__campaign__finalized=False)
            .order_by('-created_at')
        )


invoicing_history_credits = InvoicingHistoryCredits.as_view()
invoicing_history_credits.publik_authentication_resolve_user_by_nameid = False


class InvoicingCreditPDF(PayerMixin, APIView):
    permission_classes = (permissions.IsAuthenticated,)

    def get(self, request, regie_identifier, credit_identifier):
        regie = get_object_or_404(Regie, slug=regie_identifier)
        payer_external_id = self.get_payer_external_id(
            request=request,
            regie=regie,
            nameid=request.GET.get('NameID'),
            payer_external_id=request.GET.get('payer_external_id'),
        )
        credit = get_object_or_404(
            Credit.objects.exclude(pool__campaign__finalized=False),
            uuid=credit_identifier,
            regie=regie,
            payer_external_id=payer_external_id,
            date_publication__lte=now().date(),
            cancelled_at__isnull=True,
        )
        result = credit.html()
        html = HTML(string=result)
        pdf = html.write_pdf()
        response = HttpResponse(pdf, content_type='application/pdf')
        response['Content-Disposition'] = 'attachment; filename="%s.pdf"' % credit.formatted_number
        return response


invoicing_credit_pdf = InvoicingCreditPDF.as_view()
invoicing_credit_pdf.publik_authentication_resolve_user_by_nameid = False


class InvoicingRefunds(PayerMixin, APIView):
    permission_classes = (permissions.IsAuthenticated,)
    serializer_class = serializers.RefundSerializer

    def get(self, request, regie_identifier):
        regie = get_object_or_404(Regie, slug=regie_identifier)
        payer_external_id = self.get_payer_external_id(
            request=request,
            regie=regie,
            nameid=request.GET.get('NameID'),
            payer_external_id=request.GET.get('payer_external_id'),
        )
        refunds = Refund.objects.filter(
            regie=regie,
            payer_external_id=payer_external_id,
        ).order_by('-created_at')
        return Response(
            {
                'data': [
                    {
                        'id': str(refund.uuid),
                        'display_id': refund.formatted_number,
                        'amount': refund.amount,
                        'created': refund.created_at.date(),
                        'has_pdf': False,
                    }
                    for refund in refunds
                ]
            }
        )

    def post(self, request, regie_identifier):
        regie = get_object_or_404(Regie, slug=regie_identifier)
        serializer = self.serializer_class(data=request.data, regie=regie)
        if not serializer.is_valid():
            raise APIErrorBadRequest(N_('invalid payload'), errors=serializer.errors)

        credit = serializer.validated_data['credit']
        with transaction.atomic():
            refund = Refund.objects.create(
                regie=regie,
                amount=credit.remaining_amount,
                payer_external_id=credit.payer_external_id,
                payer_first_name=credit.payer_first_name,
                payer_last_name=credit.payer_last_name,
                payer_address=credit.payer_address,
                date_refund=serializer.validated_data.get('date_refund'),
            )
            refund.set_number()
            refund.save()
            CreditAssignment.objects.create(
                refund=refund,
                amount=credit.remaining_amount,
                credit=credit,
            )

        return Response(
            {
                'err': 0,
                'data': refund.get_notification_payload(request),
            }
        )


invoicing_refunds = InvoicingRefunds.as_view()
invoicing_refunds.publik_authentication_resolve_user_by_nameid = False
