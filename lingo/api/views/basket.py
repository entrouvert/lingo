# lingo - payment and billing system
# Copyright (C) 2023  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import datetime

from django.conf import settings
from django.shortcuts import get_object_or_404
from django.utils.timezone import now
from django.utils.translation import gettext as _
from django.utils.translation import gettext_noop as N_
from rest_framework import permissions
from rest_framework.views import APIView

from lingo.api import serializers
from lingo.api.utils import APIErrorBadRequest, Response
from lingo.basket.models import Basket, BasketLine, BasketLineItem
from lingo.invoicing.models import DraftInvoice, DraftInvoiceLine, Regie


class BasketsView(APIView):
    permission_classes = (permissions.IsAuthenticated,)
    serializer_class = serializers.BasketSerializer

    def post(self, request, regie_identifier):
        regie = get_object_or_404(Regie, slug=regie_identifier)
        serializer = self.serializer_class(data=request.data, regie=regie)
        if not serializer.is_valid():
            raise APIErrorBadRequest(N_('invalid payload'), errors=serializer.errors)

        basket = Basket.objects.filter(
            regie=regie,
            payer_nameid=serializer.validated_data['payer_nameid'],
            status='open',
        ).first()
        # open basket already exists in the regie, return this one
        if basket is not None:
            return Response({'data': {'basket_id': str(basket.uuid)}})

        today = now().date()
        invoice = DraftInvoice.objects.create(
            regie=regie,
            label=_('Invoice from %s') % today.strftime('%d/%m/%Y'),
            date_publication=today,
            date_payment_deadline=today + datetime.timedelta(days=1),
            date_due=today + datetime.timedelta(days=1),
            payer_demat=True,
            payer_direct_debit=False,
            **{
                k: v
                for k, v in serializer.validated_data.items()
                if k.startswith('payer') and k != 'payer_nameid'
            },
        )
        basket = Basket.objects.create(
            regie=regie,
            draft_invoice=invoice,
            expiry_at=now() + datetime.timedelta(minutes=settings.BASKET_EXPIRY_DELAY),
            **serializer.validated_data,
        )

        return Response({'data': {'basket_id': str(basket.uuid)}})


basket_baskets = BasketsView.as_view()


class BasketCheckView(APIView):
    permission_classes = (permissions.IsAuthenticated,)
    serializer_class = serializers.BasketCheckSerializer

    def get(self, request, regie_identifier):
        regie = get_object_or_404(Regie, slug=regie_identifier)
        serializer = self.serializer_class(data=request.query_params, regie=regie)
        if not serializer.is_valid():
            err_class = ''
            for errors in serializer.errors.values():
                for error in errors:
                    if error.code in [
                        'payer_active_basket',
                        'user_existing_line',
                        'payer_active_basket_to_pay',
                    ]:
                        err_class = error.code
            raise APIErrorBadRequest(N_('invalid payload'), errors=serializer.errors, err_class=err_class)

        return Response({'err': 0})


basket_basket_check = BasketCheckView.as_view()


class BasketLinesView(APIView):
    permission_classes = (permissions.IsAuthenticated,)
    serializer_class = serializers.BasketLineSerializer

    def post(self, request, regie_identifier, basket_identifier):
        basket = get_object_or_404(
            Basket, uuid=basket_identifier, regie__slug=regie_identifier, status='open'
        )
        serializer = self.serializer_class(data=request.data, basket=basket)
        if not serializer.is_valid():
            raise APIErrorBadRequest(N_('invalid payload'), errors=serializer.errors)

        params = serializer.validated_data
        reuse = params.pop('reuse')
        if reuse:
            user_external_id = params.pop('user_external_id')
            line, dummy = BasketLine.objects.get_or_create(
                basket=basket, user_external_id=user_external_id, defaults=params
            )
            if line.closed:
                line.closed = False
                line.save()
        else:
            line = BasketLine.objects.create(basket=basket, **params)

        return Response({'data': {'line_id': str(line.uuid), 'closed': line.closed}})


basket_basket_lines = BasketLinesView.as_view()


class BasketLineItemsView(APIView):
    permission_classes = (permissions.IsAuthenticated,)
    serializer_class = serializers.BasketLineItemSerializer

    def post(self, request, regie_identifier, basket_identifier, line_identifier):
        line = get_object_or_404(
            BasketLine,
            uuid=line_identifier,
            basket__uuid=basket_identifier,
            basket__status='open',
            basket__regie__slug=regie_identifier,
            closed=False,
        )
        serializer = self.serializer_class(data=request.data)
        if not serializer.is_valid():
            raise APIErrorBadRequest(N_('invalid payload'), errors=serializer.errors)

        event_slug = serializer.validated_data.pop('slug', '')
        event_label = ''
        agenda_slug = ''
        if '@' in event_slug:
            agenda_slug = event_slug.split('@')[0]
            event_label = serializer.validated_data['label']
        item = BasketLineItem.objects.create(
            line=line,
            event_slug=event_slug,
            event_label=event_label,
            agenda_slug=agenda_slug,
            **serializer.validated_data,
        )

        return Response({'data': {'item_id': str(item.uuid)}})


basket_basket_line_items = BasketLineItemsView.as_view()


class BasketLineCloseView(APIView):
    permission_classes = (permissions.IsAuthenticated,)

    def post(self, request, regie_identifier, basket_identifier, line_identifier):
        self.line = get_object_or_404(
            BasketLine,
            uuid=line_identifier,
            basket__uuid=basket_identifier,
            basket__status='open',
            basket__regie__slug=regie_identifier,
            closed=False,
        )
        self.line.closed = True
        self.line.save()
        self.generate_invoice_lines()
        basket = self.line.basket
        basket.expiry_at = now() + datetime.timedelta(minutes=settings.BASKET_EXPIRY_DELAY)
        basket.save()

        return Response({'data': {'line_id': str(self.line.uuid), 'closed': True}})

    def generate_invoice_lines(self):
        for (
            slug,
            label,
            description,
            quantity,
            unit_amount,
            dummy,
            event_date,
            event_slug,
            event_label,
            agenda_slug,
            activity_label,
            accounting_code,
            dates,
        ) in self.line.formatted_items:
            DraftInvoiceLine.objects.create(
                invoice=self.line.basket.draft_invoice,
                event_date=event_date,
                slug=slug,
                event_slug=event_slug,
                event_label=event_label,
                agenda_slug=agenda_slug,
                activity_label=activity_label,
                label=label,
                description=description,
                payer_external_id=self.line.basket.payer_external_id,
                payer_first_name=self.line.basket.payer_first_name,
                payer_last_name=self.line.basket.payer_last_name,
                payer_address=self.line.basket.payer_address,
                payer_demat=True,
                payer_direct_debit=False,
                user_external_id=self.line.user_external_id,
                user_first_name=self.line.user_first_name,
                user_last_name=self.line.user_last_name,
                quantity=quantity,
                unit_amount=unit_amount,
                accounting_code=accounting_code,
                form_url=self.line.form_url,
                details={'dates': dates},
            )


basket_basket_line_close = BasketLineCloseView.as_view()


class BasketLineCancelView(APIView):
    permission_classes = (permissions.IsAuthenticated,)

    def post(self, request, regie_identifier, basket_identifier, line_identifier):
        line = get_object_or_404(
            BasketLine,
            uuid=line_identifier,
            basket__uuid=basket_identifier,
            basket__status='open',
            basket__regie__slug=regie_identifier,
            closed=False,
        )
        line.items.all().delete()
        line.delete()

        return Response({'err': 0})


basket_basket_line_cancel = BasketLineCancelView.as_view()
