from unittest import mock

import pytest
from django.db import connection
from django.test.utils import CaptureQueriesContext

from lingo.invoicing.models import Payer, Regie
from lingo.snapshot.models import PayerSnapshot
from tests.invoicing.utils import mocked_requests_send
from tests.utils import login

pytestmark = pytest.mark.django_db


@mock.patch('requests.Session.send', side_effect=mocked_requests_send)
def test_add_payer(mock_send, app, admin_user):
    app = login(app)
    resp = app.get('/manage/invoicing/regies/')
    resp = resp.click('Payer Settings')
    resp = resp.click('New payer')
    assert resp.context['form'].fields['carddef_reference'].widget.choices == [
        ('', '-----'),
        ('default:card_model_1', 'Card Model 1'),
        ('default:card_model_2', 'Card Model 2'),
        ('default:card_model_3', 'Card Model 3'),
    ]
    resp.form['label'] = 'Foo bar'
    resp.form['description'] = 'a little description'
    resp.form['carddef_reference'] = 'default:card_model_1'
    resp.form['payer_external_id_prefix'] = 'payer:'
    resp.form['payer_external_id_template'] = 'bar'
    resp.form['payer_external_id_from_nameid_template'] = 'foo'
    resp = resp.form.submit()
    payer = Payer.objects.latest('pk')
    assert resp.location.endswith('/manage/invoicing/payer/%s/' % payer.pk)
    assert payer.label == 'Foo bar'
    assert payer.slug == 'foo-bar'
    assert payer.description == 'a little description'
    assert payer.carddef_reference == 'default:card_model_1'
    assert payer.payer_external_id_prefix == 'payer:'
    assert payer.payer_external_id_template == 'bar'
    assert payer.payer_external_id_from_nameid_template == 'foo'
    assert PayerSnapshot.objects.count() == 1


@mock.patch('requests.Session.send', side_effect=mocked_requests_send)
def test_edit_payer(mock_send, app, admin_user):
    payer = Payer.objects.create(label='Foo bar', carddef_reference='foo:bar')
    payer2 = Payer.objects.create(label='baz')

    app = login(app)
    resp = app.get('/manage/invoicing/payers/')
    resp = resp.click(href='/manage/invoicing/payer/%s/' % payer.pk)
    resp = resp.click(href='/manage/invoicing/payer/%s/edit/' % payer.pk)
    assert resp.context['form'].fields['carddef_reference'].widget.choices == [
        ('', '-----'),
        ('default:card_model_1', 'Card Model 1'),
        ('default:card_model_2', 'Card Model 2'),
        ('default:card_model_3', 'Card Model 3'),
    ]
    resp.form['label'] = 'Foo bar baz'
    resp.form['slug'] = payer2.slug
    resp.form['carddef_reference'] = 'default:card_model_1'
    resp = resp.form.submit()
    assert resp.context['form'].errors['slug'] == ['Another payer exists with the same identifier.']

    resp.form['slug'] = 'baz2'
    resp = resp.form.submit()
    assert resp.location.endswith('/manage/invoicing/payer/%s/' % payer.pk)
    payer.refresh_from_db()
    assert payer.label == 'Foo bar baz'
    assert payer.slug == 'baz2'
    assert payer.carddef_reference == 'default:card_model_1'
    assert PayerSnapshot.objects.count() == 1


@mock.patch('requests.Session.send', side_effect=mocked_requests_send)
def test_edit_payer_mapping(mock_send, app, admin_user):
    payer = Payer.objects.create(label='Foo bar')

    app = login(app)
    resp = app.get('/manage/invoicing/payer/%s/' % payer.pk)
    assert '/manage/invoicing/payer/%s/mapping/' % payer.pk not in resp
    payer.carddef_reference = 'default:card_model_1'
    payer.save()
    resp = app.get('/manage/invoicing/payer/%s/' % payer.pk)
    resp = resp.click(href='/manage/invoicing/payer/%s/mapping/' % payer.pk)
    assert len(resp.context['form'].fields) == 5
    choices = [('', '-----'), ('fielda', 'Field A'), ('fieldb', 'Field B')]
    assert resp.context['form'].fields['first_name'].choices == choices
    assert resp.context['form'].fields['last_name'].choices == choices
    assert resp.context['form'].fields['address'].choices == choices
    assert resp.context['form'].fields['demat'].choices == choices
    assert resp.context['form'].fields['direct_debit'].choices == choices
    resp.form['first_name'] = 'fielda'
    resp.form['last_name'] = 'fieldb'
    resp.form['address'] = 'fieldb'
    resp = resp.form.submit()
    assert resp.location.endswith('/manage/invoicing/payer/%s/#open:mapping' % payer.pk)
    payer.refresh_from_db()
    assert payer.user_fields_mapping == {
        'first_name': 'fielda',
        'last_name': 'fieldb',
        'address': 'fieldb',
        'demat': '',
        'direct_debit': '',
    }
    resp = app.get('/manage/invoicing/payer/%s/mapping/' % payer.pk)
    assert resp.form['first_name'].value == 'fielda'
    assert resp.form['last_name'].value == 'fieldb'
    assert resp.form['address'].value == 'fieldb'
    assert PayerSnapshot.objects.count() == 1


def test_delete_payer(app, admin_user):
    payer = Payer.objects.create(label='Foo bar')

    app = login(app)
    resp = app.get('/manage/invoicing/payer/%s/' % payer.pk)
    resp = resp.click(href='/manage/invoicing/payer/%s/delete/' % payer.pk)
    resp = resp.form.submit()
    assert resp.location.endswith('/manage/invoicing/payers/')
    assert Payer.objects.exists() is False
    assert PayerSnapshot.objects.count() == 1

    # can not delete payer used in regie
    payer.save()
    Regie.objects.create(label='foo', payer=payer)
    resp = app.get('/manage/invoicing/payer/%s/' % payer.pk)
    assert '/manage/invoicing/payer/%s/delete/' % payer.pk not in resp
    resp = app.get('/manage/invoicing/payer/%s/delete/' % payer.pk, status=404)


@mock.patch('requests.Session.send', side_effect=mocked_requests_send)
def test_detail_payer(mock_send, app, admin_user):
    payer = Payer.objects.create(label='Foo bar')
    app = login(app)
    resp = app.get('/manage/invoicing/payer/%s/' % payer.pk)
    assert 'Card Model: <code></code>' in resp

    payer.carddef_reference = 'unknown:unknown'
    payer.user_fields_mapping = {
        'first_name': 'fielda',
        'last_name': 'fieldb',
        'address': 'fieldb',
        'demat': '',
        'direct_debit': '',
    }
    payer.payer_external_id_prefix = 'payer:'
    payer.payer_external_id_template = 'bar'
    payer.payer_external_id_from_nameid_template = 'foo'
    payer.save()
    resp = app.get('/manage/invoicing/payer/%s/' % payer.pk)
    assert 'Card Model: <code></code>' in resp
    assert 'Prefix for payer external id: <pre>payer:</pre>' in resp
    assert 'Template for payer external id: <pre>bar</pre>' in resp
    assert 'Template for payer external id from nameid: <pre>foo</pre>' in resp

    payer.carddef_reference = 'default:card_model_1'
    payer.save()
    resp = app.get('/manage/invoicing/payer/%s/' % payer.pk)
    assert 'Card Model: <code>Card Model 1</code>' in resp
    assert resp.pyquery('dl dt').text() == 'First name: Last name: Address: Demat: Direct debit:'
    assert resp.pyquery('dl dd').text() == 'Field A Field B Field B  '


def test_payer_inspect(app, admin_user):
    payer = Payer.objects.create(label='Foo bar')

    app = login(app)
    resp = app.get('/manage/invoicing/payer/%s/' % payer.pk)
    with CaptureQueriesContext(connection) as ctx:
        resp = resp.click(href='/manage/invoicing/payer/%s/inspect/' % payer.pk)
        assert len(ctx.captured_queries) == 3
