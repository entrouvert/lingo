import copy

import pytest
from django.contrib.auth.models import Group

from lingo.invoicing.models import Payer, PaymentType, Regie
from lingo.invoicing.utils import export_site, import_site
from lingo.utils.misc import LingoImportError

pytestmark = pytest.mark.django_db


def test_import_export(app):
    Regie.objects.create(label='Foo Bar')
    Payer.objects.create(label='Foo Bar')

    data = export_site()
    assert len(data['regies']) == 1
    assert len(data['payers']) == 1
    import_site(data={})
    assert Regie.objects.count() == 1
    assert Payer.objects.count() == 1


def test_import_export_regies(app):
    payload = export_site()
    assert len(payload['regies']) == 0

    group1 = Group.objects.create(name='role-foo-1')
    group2 = Group.objects.create(name='role-foo-2')
    group3 = Group.objects.create(name='role-foo-3')
    group4 = Group.objects.create(name='role-foo-4')
    regie = Regie.objects.create(
        label='Foo bar',
        description='blah',
        edit_role=group1,
        view_role=group2,
        invoice_role=group3,
        control_role=group4,
        assign_credits_on_creation=False,
        counter_name='{yyyy}',
        invoice_number_format='Fblah{regie_id:02d}-{yy}-{mm}-{number:07d}',
        payment_number_format='Rblah{regie_id:02d}-{yy}-{mm}-{number:07d}',
        docket_number_format='Bblah{regie_id:02d}-{yy}-{mm}-{number:07d}',
        credit_number_format='Ablah{regie_id:02d}-{yy}-{mm}-{number:07d}',
        refund_number_format='Vblah{regie_id:02d}-{yy}-{mm}-{number:07d}',
        main_colour='#DF5A14',
        invoice_model='full',
        invoice_custom_text='foo bar',
        certificate_model='full',
        controller_name='Foo',
        city_name='Bar',
    )

    payload = export_site()
    assert len(payload['regies']) == 1

    regie.delete()
    Group.objects.all().delete()
    assert not Regie.objects.exists()

    Group.objects.create(name='role')
    with pytest.raises(LingoImportError) as excinfo:
        import_site(copy.deepcopy(payload))
    assert str(excinfo.value) == 'Missing role: role-foo-1'

    group1 = Group.objects.create(name='role-foo-1')
    with pytest.raises(LingoImportError) as excinfo:
        import_site(copy.deepcopy(payload))
    assert str(excinfo.value) == 'Missing role: role-foo-2'

    group2 = Group.objects.create(name='role-foo-2')
    with pytest.raises(LingoImportError) as excinfo:
        import_site(copy.deepcopy(payload))
    assert str(excinfo.value) == 'Missing role: role-foo-3'

    group3 = Group.objects.create(name='role-foo-3')
    with pytest.raises(LingoImportError) as excinfo:
        import_site(copy.deepcopy(payload))
    assert str(excinfo.value) == 'Missing role: role-foo-4'

    group4 = Group.objects.create(name='role-foo-4')
    import_site(copy.deepcopy(payload))
    assert Regie.objects.count() == 1
    regie = Regie.objects.first()
    assert regie.label == 'Foo bar'
    assert regie.slug == 'foo-bar'
    assert regie.edit_role == group1
    assert regie.view_role == group2
    assert regie.invoice_role == group3
    assert regie.control_role == group4
    assert regie.assign_credits_on_creation is False
    assert regie.counter_name == '{yyyy}'
    assert regie.invoice_number_format == 'Fblah{regie_id:02d}-{yy}-{mm}-{number:07d}'
    assert regie.payment_number_format == 'Rblah{regie_id:02d}-{yy}-{mm}-{number:07d}'
    assert regie.docket_number_format == 'Bblah{regie_id:02d}-{yy}-{mm}-{number:07d}'
    assert regie.credit_number_format == 'Ablah{regie_id:02d}-{yy}-{mm}-{number:07d}'
    assert regie.refund_number_format == 'Vblah{regie_id:02d}-{yy}-{mm}-{number:07d}'
    assert regie.main_colour == '#DF5A14'
    assert regie.invoice_model == 'full'
    assert regie.invoice_custom_text == 'foo bar'
    assert regie.certificate_model == 'full'
    assert regie.controller_name == 'Foo'
    assert regie.city_name == 'Bar'

    # update
    update_payload = copy.deepcopy(payload)
    update_payload['regies'][0]['label'] = 'Foo bar Updated'
    import_site(update_payload)
    regie.refresh_from_db()
    assert regie.label == 'Foo bar Updated'

    # insert another regie
    regie.slug = 'foo-bar-updated'
    regie.save()
    import_site(copy.deepcopy(payload))
    assert Regie.objects.count() == 2
    regie = Regie.objects.latest('pk')
    assert regie.label == 'Foo bar'
    assert regie.slug == 'foo-bar'


def test_import_export_regie_with_payer(app):
    payer = Payer.objects.create(label='Foo')
    regie = Regie.objects.create(label='Bar', payer=payer)
    data = export_site()

    regie.payer = None
    regie.save()
    payer.delete()
    del data['payers']

    with pytest.raises(LingoImportError) as excinfo:
        import_site(data)
    assert str(excinfo.value) == 'Missing "foo" payer'

    Payer.objects.create(label='Foobar')
    with pytest.raises(LingoImportError) as excinfo:
        import_site(data)
    assert str(excinfo.value) == 'Missing "foo" payer'

    payer = Payer.objects.create(label='Foo')
    import_site(data)
    regie.refresh_from_db()
    assert regie.payer == payer


def test_import_export_regie_with_payment_types(app):
    payload = export_site()
    assert len(payload['regies']) == 0

    regie = Regie.objects.create(label='Foo bar')
    PaymentType.objects.create(label='Foo', regie=regie)
    PaymentType.objects.create(label='Baz', regie=regie)

    payload = export_site()
    assert len(payload['regies']) == 1

    PaymentType.objects.all().delete()
    regie.delete()
    assert not Regie.objects.exists()
    assert not PaymentType.objects.exists()

    import_site(copy.deepcopy(payload))
    assert Regie.objects.count() == 1
    regie = Regie.objects.first()
    assert regie.label == 'Foo bar'
    assert regie.slug == 'foo-bar'
    assert regie.paymenttype_set.count() == 2
    assert PaymentType.objects.get(regie=regie, label='Foo', slug='foo')
    assert PaymentType.objects.get(regie=regie, label='Baz', slug='baz')

    # update
    update_payload = copy.deepcopy(payload)
    update_payload['regies'][0]['label'] = 'Foo bar Updated'
    import_site(update_payload)
    regie.refresh_from_db()
    assert regie.label == 'Foo bar Updated'

    # insert another regie
    regie.slug = 'foo-bar-updated'
    regie.save()
    import_site(copy.deepcopy(payload))
    assert Regie.objects.count() == 2
    regie = Regie.objects.latest('pk')
    assert regie.label == 'Foo bar'
    assert regie.slug == 'foo-bar'
    assert regie.paymenttype_set.count() == 2
    assert PaymentType.objects.get(regie=regie, label='Foo', slug='foo')
    assert PaymentType.objects.get(regie=regie, label='Baz', slug='baz')


def test_import_export_payers(app):
    payload = export_site()
    assert len(payload['payers']) == 0

    payer = Payer.objects.create(label='Foo bar', user_fields_mapping={'foo': 'bar'})

    payload = export_site()
    assert len(payload['payers']) == 1

    payer.delete()
    assert not Payer.objects.exists()

    import_site(copy.deepcopy(payload))
    assert Payer.objects.count() == 1
    payer = Payer.objects.first()
    assert payer.label == 'Foo bar'
    assert payer.slug == 'foo-bar'
    assert payer.user_fields_mapping == {'foo': 'bar'}

    # update
    update_payload = copy.deepcopy(payload)
    update_payload['payers'][0]['label'] = 'Foo bar Updated'
    import_site(update_payload)
    payer.refresh_from_db()
    assert payer.label == 'Foo bar Updated'

    # insert another payer
    payer.slug = 'foo-bar-updated'
    payer.save()
    import_site(copy.deepcopy(payload))
    assert Payer.objects.count() == 2
    payer = Payer.objects.latest('pk')
    assert payer.label == 'Foo bar'
    assert payer.slug == 'foo-bar'
    assert payer.user_fields_mapping == {'foo': 'bar'}
